##Rent Platform

Website is built on Flask. All the python packages are mentioned in requirements.txt  

####Installation instructions  

**Setting up developer environment**

+ Clone/Download code from repo
+ ```git clone git@bitbucket.org:silvercreative/rentplatform.git```
+ Run build_sys.sh in your terminal
+ Setup a virtual environment
+ ```virtualenv pyenvs/rentplatform```
+ Install all the packages from requirements.txt file
+ ```pip install -r requirements.txt```
+ Setup the database

**Exporting and Importing Data from Mongodb**  

+ get database dump. cmd to get dump:
+ ```mongodump --db dbname -o out_dir```
+ importing data  
+ ```mongorestore --db dbname data_dir```

####Live Server information

start the parnet process if killed:

```cd rentplatform; python /usr/local/bin/gunicorn serve:app -b 127.0.0.2:8001 --daemon --error-log=error.log```


To run the application keeping the logger enabled

```/home/vertis/pyenvs/rentplatform/bin/python /home/vertis/pyenvs/rentplatform/bin/gunicorn serve:app -b 127.0.0.2:8001 --daemon  --error-log=error.log```


How to deploy the project
=========================

This project can automatically be deployed to silvercreativelabspace.com via the following commands:

```fab deploy:silvercreativelabspace.com,master```

The ```master``` branch can be switched out with any branch that has been pushed to origin.


How to run the project
=====================

1. cd into the project dir 
2. activate the virtualenv ```pyenvs/rentplatform/bin/activate```
3. run: ```python starter.py runserver```

python shell can be activated by following cmd
python starter.py shell

How to take database backup
==========================
Below are the cmds to dump and restore mongodb database

cmd to get dump:
```mongodump --db dbname -o out_dir```


Exporting and Importing Data from Mongodb
============================
get database dump

cmd to get dump:
```mongodump --db dbname -o out_dir```

importing data:

```mongorestore --db dbname data_dir```

Restarting the broken Mongodb  
=============================  

```
rm /var/lib/mongodb/mongod.lock
mongod --repair
service mongodb start
service mongodb status
```  


Adding User Authentication to mongoDB  
=============================  

- Create the user administrator 
        
        $ mongo
        
    And type below:
        
        use admin
        db.createUser({user:"myUserAdmin", pwd:"alpacadynowizardfart", roles: [{role:"userAdminAnyDatabase", db:"admin"}]})
        
    Exit and restart service
        
        $ sudo service mongod restart
        
- Enable authentication.
        
        $ sudo nano /etc/mongod.conf
  
    Go to `net` session and change the port and bindIp.
    
        net:
          port: 32156
          bindIp: 127.0.0.1
      
    (As you could see above, mongodb uses port 32156 in default, but it is **highly** recommended to use custom port.)
    
    So, let us use `32156` then!
    
    Go to `security` session and change `authorization: enabled` to `authorization: disabled`.  
        
        security:
          authorization: enabled
  
    After saving & exiting, restart mongodb server with `sudo service mongod restart`
    
- Create general DB user account.
    
        $ mongo --port 32156 -u "myUserAdmin" -p "alpacadynowizardfart" --authenticationDatabase "admin"
    
    And type below:
    
        use rentplatform
        db.createUser({user: "admin_aptsfeed", pwd: "f1al4*vnaNv732Q0P", roles: ["dbAdmin", "readWrite"]})
        
    And restart `mongod` service
        
        sudo service mongod restart
        
    
- Change our Flask app to pass authentication.
    
    Remove the current version(2.5.2) of `pymongo` and install 2.8.
    
        pip install --upgrade pymongo==2.8
      
    Change the `serve.py` & `mobile.py`
        
        app.config["MONGODB_SETTINGS"] = {'DB': 'rentplatform',
                                          'port': 32156,
                                          'username': 'admin_aptsfeed',
                                          'password': 'f1al4*vnaNv732Q0P'}
    
- Restart Server
    
        sudo service apache2 restart
