# set the path
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from flask.ext.script import Manager, Server
from serve import app
from commands import FetchPricingCommand, FTPPriceCommand

app.debug = True
manager = Manager(app)

# Turn on debugger by default and reloader
manager.add_command("runserver", Server(
    use_debugger=True,
    use_reloader=True,
    host='0.0.0.0')
                    )
manager.add_command("fetch_pricing", FetchPricingCommand())
manager.add_command("ftp_pricing", FTPPriceCommand())


if __name__ == "__main__":
    manager.run()
