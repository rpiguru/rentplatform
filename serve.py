from flask import Flask, g
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager, current_user
from flask.ext.mail import Mail
from flask.ext.uploads import (UploadSet, configure_uploads, IMAGES, DOCUMENTS,
                              UploadNotAllowed)
from flask_errormail import mail_on_500

import os
from flask.templating import render_template

APPLICATION_DIR = os.path.abspath(os.path.dirname(__file__))
ADMINS = ['tim@silvercreativegroup.com']

app = Flask(__name__)
app.debug = False
app.config["MONGODB_SETTINGS"] = {'DB' : 'rentplatform'}

# DEV
app.config["UPLOADED_FLOORPLANS_DEST"] = os.path.join(APPLICATION_DIR, 'static/uploads/floorplans')
app.config["UPLOADED_BUILDINGSOLO_DEST"] = os.path.join(APPLICATION_DIR, 'static/uploads/buildingsolo')
app.config["UPLOADED_BUILDINGBGS_DEST"] = os.path.join(APPLICATION_DIR, 'static/uploads/buildingbgs')
app.config["UPLOADED_BUILDINGLOGOS_DEST"] = os.path.join(APPLICATION_DIR, 'static/uploads/buildinglogos')
app.config["UPLOADED_BUILDINGPHOTOS_DEST"] = os.path.join(APPLICATION_DIR, 'static/uploads/buildingphotos')
app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"] = os.path.join(APPLICATION_DIR, 'static/uploads/residentialphotos')
app.config['UPLOADED_GALLERYIMAGES_DEST'] = os.path.join(APPLICATION_DIR, 'static/uploads/gallery')
app.config['UPLOADED_PHOTOSIMAGES_DEST'] = os.path.join(APPLICATION_DIR, 'static/uploads/photos')

# ablankusernamefordev@gmail.com
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_USERNAME'] = 'silvercreative.webmaster@gmail.com'
app.config['MAIL_PASSWORD'] = '$!lverCr3@t!v='
app.config['DEFAULT_MAIL_SENDER'] = 'silvercreative.webmaster@gmail.com'

app.config['CONTACT_AGENT_MAIL'] = ''
app.config['CONTACT_AGENT_NAME'] = ''

app.config["SECRET_KEY"] = "CXlEmHBVCapK/UWh~*$Pr}yTU#ZtBu0#>@F<L+sQ~*Fou]xM.l"
app.config["SALT"] = 'L/&m;E/n:8*2>mbP7}*W|5A%7S?yO4Y<vvv&-OEcmK{/[FoF1X'

# Feed configurations
app.config["FEEDS"] = {
    "YIELDSTAR": {
        "username": "silverCreativeGp",
        "password": "",
        "client_name": "",
        "integrations": {},
        "wsdl": "https://rmsws.mpfyieldstar.com/rmsws/AppExchange?wsdl",
        "location": "https://rmsws.yieldstar.com:443/rmsws/AppExchange",
        "host": "rmsws.yieldstar.com:443"
    },
    "FTPPriceFeed": {"integrations": {
        "New York by Gehry": {
            "directory": os.path.join(APPLICATION_DIR, '/sftp/lro_upload/incoming/'),
            "extension": ".lro",
            "community_id": "8SPRUCE",
        },
    }},
}

db = MongoEngine(app)
mail = Mail(app)

floorplans = UploadSet('floorplans', IMAGES + ('pdf',))
buildingsolo = UploadSet('buildingsolo', IMAGES)
buildingbgs = UploadSet('buildingbgs', IMAGES)
buildinglogos = UploadSet('buildinglogos', IMAGES)
buildingpics = UploadSet('buildingphotos', IMAGES)
residentialpics = UploadSet('residentialphotos', IMAGES)
galleryimages = UploadSet('galleryimages', IMAGES)
photosimages = UploadSet('photosimages', IMAGES)

configure_uploads(app, (
    buildingpics, residentialpics, buildinglogos, buildingbgs,
    buildingsolo, floorplans, galleryimages, photosimages))

def register_blueprints():
    # from views.users import users
    from views.manager import managers
    from views.home import home
    from views.users import users
    app.register_blueprint(users)
    app.register_blueprint(managers)
    app.register_blueprint(home)

def init_login():
    from models import User
    login_manager = LoginManager()
    login_manager.setup_app(app)
    login_manager.login_view = "/login/"
    def load_user(user_id):
        return User.objects(id=user_id).first()

    @app.before_request
    def before_request():
        g.user = current_user
    
    login_manager.user_loader(load_user)

    
@app.context_processor
def add_constants():
    """
    Add MEDIA_URL and STATIC_URL to tempaltes
    """
    import datetime
    from models import Building
    return dict(MEDIA_URL="",
                STATIC_URL="/static/",
                copyright_date=datetime.datetime.today(),
                FOOTER_BUILDINGS=Building.objects.all().order_by("order"),
                SITE_TITLE=os.environ.get("RENT_PLATFORM_SITE_TITLE", "RentPlatform"))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

mail_on_500(app, ADMINS)

register_blueprints()
init_login()
