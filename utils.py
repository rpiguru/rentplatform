import os
import shutil
import uuid

from models import ResidentialListing, Gallery, Photos
from serve import app


def clone_listing(original):
    """Helper method that clones all properties of a ResidentialListing
    except for photo_gallery, specs and floorplan."""
    new = ResidentialListing(
        building=original.building,
        floor=original.floor,
        apt_num=original.apt_num,
        price=original.price,
        description=original.description,
        bed=original.bed,
        bath=original.bath,
        sqft=original.sqft,
        avail=original.avail,
        notes=original.notes,
        availability=original.availability,
        modified_date=original.modified_date,
        specs=original.specs,
    )
    return new


def clone_gallery(original):
    new_gallery = Gallery(title=original.title)
    new_gallery.photos = []
    for photo in original.photos:
        new_gallery.photos.append(clone_photo(photo))
    new_gallery.save()
    return new_gallery


def clone_photo(original):
    new_photo = None
    if original.file_path:
        new_photo = Photos(
            title=original.title,
            name=original.name,
            active=original.active,
            created_time=original.created_time,
        )
        new_photo.save()
        new_photo.path = os.path.join(str(new_photo.id), original._filename)
        os.makedirs(os.path.dirname(new_photo.file_path))
        shutil.copy2(original.file_path, new_photo.file_path)
        new_photo.save()
    return new_photo


def clone_floorplan(floorplan):
    new_name = u"{}_{}".format(
        str(uuid.uuid4()).replace('-', '')[:16], floorplan)
    src = os.path.join(app.config["UPLOADED_FLOORPLANS_DEST"], floorplan)
    dst = os.path.join(app.config["UPLOADED_FLOORPLANS_DEST"], new_name)
    shutil.copy2(src, dst)
    return new_name
