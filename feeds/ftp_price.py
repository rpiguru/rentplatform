import os
from datetime import datetime

import xmltodict

from serve import app


class FTPPriceFeed(object):
    @classmethod
    def settings(cls, name):
        return app.config["FEEDS"]["FTPPriceFeed"]['integrations'].get(name)

    def process(self, building_name):
        settings = self.settings(building_name)
        if settings:
            community_id = settings['community_id']
            directory = settings['directory']
            extension = settings['extension']
            f = self.find_file(directory, extension)
            if f:
                return f, self.parse_rents(f, community_id)
        return None, None

    @staticmethod
    def find_file(directory, extension):
        if not os.path.isdir(directory):
            return False
        for f in os.listdir(directory):
            if f.lower().endswith(extension.lower()):
                return os.path.join(directory, f)
        return None

    @staticmethod
    def parse_rents(xml_file, community_id):
        if not xml_file:
            return None
        rents = {}
        with open(xml_file) as reader:
            community = xmltodict.parse(reader.read())['community']
            if community['@ID'] != community_id:
                return None

            for unit in community['units']['unit']:
                unit_id = unit["@ID"]
                for term in unit["offeredterm"]:
                    if term['@LT'] == "12":
                        rents[unit_id] = int(float(term['@EFFECTIVERENT']))
                        break
        return rents

    @staticmethod
    def cleanup_file(xml_file):
        name = "{}_{}".format(datetime.now().strftime("%Y%m%d%H%M%S"), os.path.basename(xml_file))
        dir = os.path.dirname(xml_file)
        dest_dir = os.path.join(dir, "processed")
        if not os.path.isdir(dest_dir):
            os.mkdir(dest_dir)
        os.rename(xml_file, os.path.join(dest_dir, name))
