import base64
from datetime import datetime
import sys

from pysimplesoap.client import SoapClient
import pytz

from serve import app

from logging import getLogger
logger = getLogger(__name__)


class YieldStarFeed(object):
    key = "yieldstar"

    @property
    def _settings(self):
        return app.config["FEEDS"]["YIELDSTAR"]

    def get_price_for_unit(self, unit_number):
        data = self.get_lease_term_rent(unit_number)
        if data is None:
            return None
        return self.parse_get_lease_term_rent(data)

    def __init__(self, property_id):
        self.property_id = property_id
        encoded = base64.b64encode('{}:{}'.format(
            self._settings['username'],
            self._settings['password']))
        self.client = SoapClient(
            wsdl=self._settings['wsdl'],
            http_headers={
                'Authorization': 'Basic {}'.format(encoded),
                'SOAPAction': ' ',
                'Host': self._settings['host'],
            },
            location=self._settings['location'],
            ns=False)
        try:
            # This sets up the client to make calls with .call
            # Ideally we would use this, but it doesn't work readily.
            self.client.getProperties(
                request={'clientName': self._settings['client_name']})
        except ValueError:
            pass

    def get_units(self):
        return self.client.call("getUnits", request={
            'clientName': self._settings['client_name'],
            'externalPropertyId': self.property_id,
        })

    def get_lease_term_rent(self, unit_number):
        lease_rent_request = {
            'unitNumber': unit_number,
            'minLeaseTerm': 12,
            'maxLeaseTerm': 12,
        }
        try:
            response = self.client.call("getLeaseTermRent", request={
                'clientName': self._settings['client_name'],
                'externalPropertyId': self.property_id,
                'leaseTermRentUnitRequest': lease_rent_request,
            })
        except Exception as err:
            sys.stderr.write("Error getting lease term rent for {}".format(unit_number))
            sys.stderr.write("{}".format(err))
            sys.stderr.write(self.client.xml_response)
            response = None
        return response

    def parse_get_lease_term_rent(self, response):
        try:
            unit_rates = response("S:Body")("getLeaseTermRentResponse")("return")("leaseTermRentUnitResponse")("unitRate")
        except AttributeError:
            # This unit isn't available.
            return None
        today = datetime.utcnow().replace(tzinfo=pytz.UTC).astimezone(pytz.timezone("US/Eastern")).date()

        for unit_rate in unit_rates:
            move_in = datetime.strptime(str(unit_rate("moveInDate")), "%Y-%m-%d").date()
            if (move_in - today).days in [6, 7]:
                return int(unit_rate("finalRent"))
        return None
