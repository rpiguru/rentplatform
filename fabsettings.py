# Check fabfile.py for defaults.

FABRIC = {
    "SSH_USER": "dpsmedia-staging", # SSH username
    "SSH_PASS":  "dpsmedia*", # SSH password (consider key-based authentication)
    "SSH_KEY_PATH":  "", # Local path to SSH key file, for key-based auth
    "HOSTS": ["10.10.10.196"], # List of hosts to deploy to
    "VIRTUALENV_HOME":  "/home/dpsmedia-staging/pyenvs", # Absolute remote path for virtualenvs
    "PROJECT_NAME": "rentplatform", # Unique identifier for project
    "PROJECT_PATH": "/home/dpsmedia-staging/pyprojects/rentplatform",
    "REQUIREMENTS_PATH": "requirements.txt", # Path to pip requirements, relative to project path
    "LOCALE": "en_US.UTF-8", # Should end with ".UTF-8"
    "LIVE_HOSTNAME": "www.example.com", # Host for public site.
    "REPO_URL": "ssh://git@10.10.10.80:7999/SC/rentplatform.git", # Git or Mercurial remote repo URL for the project
    "DB_PASS": "algin", # Live database password
    "ADMIN_PASS": "admin", # Live admin user password
    
    "APP_DIR" :"rentplatform",  #within the repo location where the django application
    "PROJECT_LOCATION" : "/home/dpsmedia-staging/pyprojects", #location where you want to import the code eg: workspace
    "SYSTEM_PACKAGES" : ["build-essential", "pkg-config",  "python-dev",  "git"]

}
