from serve import app
from models import Borough, BuildingType
BuildingType.objects.delete()
Borough.objects.delete()
BuildingType(name='High Rise').save()
Borough(name='Manhattan').save()
Borough(name='Brooklyn').save()
Borough(name='Bronx').save()
Borough(name='Queens').save()
Borough(name='Staten Island').save()
