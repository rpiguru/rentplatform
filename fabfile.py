from __future__ import with_statement
from fabric.api import local, run, cd, task, get, env, sudo, lcd, execute
import os
import datetime
import random
import shutil
import time



TMP_DIR = "/tmp"

env_settings = {
    "silvercreativelabspace.com":{
        "app_dir": "/var/www/pyapps/rentplatform/",
        "dump_dir": "/var/www/pyapps/rentplatform/datadump/",
        "app_user": "www-data",
        "user": "silvercreative",
        "password": "fOU34dl23f8vDFlknqpxvc*)32413",
        "app_port": "8004",
        "run_cmd": "/var/www/pyenvs/rentplatform/bin/gunicorn serve:app -b 127.0.0.2:8004 --daemon -w4 --log-file=out.log --error-log=error.log",
    },
    "aptsfeed.com":{
        "app_dir": "/var/www/vhosts/aptsfeed.com/pyapps/rentplatform/",
        "app_user": "admin_aptsfeed",
        "user": "admin_aptsfeed",
        "password": "f1al4*vnaNv732Q0P",
        "app_port": None,
        "run_cmd": "service apache2 restart",
        "pip_cmd": "/var/www/vhosts/aptsfeed.com/pyenvs/rentplatform/bin/pip install -r requirements.txt",
        "python": "/var/www/vhosts/aptsfeed.com/pyenvs/rentplatform/bin/python",
    },
}


def set_env(hostname):
    env_vals = env_settings.get(hostname)
    for k, v in env_vals.items():
        env[k] = v


def create_sandbox():
    while(True):
        dir = str(random.random())[2:]
        path = "/{}/{}".format(TMP_DIR, dir)
        if not os.path.exilsofsts(path): break

    os.mkdir(path)
    return path


def remove_sandbox(path):
    if os.path.exists(path):
        shutil.rmtree(path)


@task
def prepare_datadump():
    """
    Takes the database dump
    """
    host = env["host"]
    set_env(host)

    dump_dir_name = datetime.datetime.strftime(datetime.datetime.today(), "%b_%d")
    dump_dir = os.path.join(env["dump_dir"], dump_dir_name)
    code_dir = env["app_dir"]
    run("mkdir -p {}".format(dump_dir))
    with cd(dump_dir):
        run("mongodump --db rentplatform -o db")
        media_dir = os.path.join(code_dir, "static", "uploads")
        run("cp -rf {} uploads".format(media_dir, ))

    dump_dir_parent = os.path.dirname(dump_dir)
    with cd(dump_dir_parent):
        filename = "{}.tar.bz2".format(dump_dir_name)
        run("tar cjvf {} {}".format(filename, dump_dir_name))
        run("rm -rf {}".format(dump_dir_name))
        print "dump file: {}".format(os.path.join(dump_dir_parent, filename))


@task
def download_dump(dump_file, local_dump_location):
    """
    downloads the datadump from server to local machine
    @dump_file: name of the file that needs to be downloaded. this assumes that the file is in the 
    "/home/jacob/datadump/pistilli/" dir
    @local_dump_location: abs path to the location machine where the file should be downloaded
    """
    host = env["host"]
    set_env(host)

    file = os.path.join(env["dump_dir"], dump_file)
    with cd(local_dump_location):
        get(file)


@task
def deploy(host, branch="master"):
    """
    Deploys the code to server. by default on master branch
    @branch: name of the branch that we want to push
    """
    env.hosts = host
    set_env(host)
    execute(code_update, branch)
    execute(reload_server)


def code_update(branch):
    with cd(env["app_dir"]):
        sudo("git fetch", user=env["app_user"])
        sudo("git checkout {}".format(branch), user=env["app_user"])
        sudo("git pull origin {}".format(branch), user=env["app_user"])


@task
def reload_server(host=None):
    """
    Reloads the application server. Here it kills the gunicorn process and starts the new process
    """
    if host:
        env.hosts = host
        set_env(host)
    with cd(env["app_dir"]):
        if env["app_port"]:
            sudo("lsof -ti:{} | xargs -r kill".format(env["app_port"]))
        if env['app_user'] == env['user']:
            sudo(env['run_cmd'])
        else:
            sudo(env["run_cmd"], user=env["app_user"], pty=False)


@task
def load_dump(dumpfile):
    """
    Loads the dump file in the system. 
    This dump file should be d one created by prepare_datadump cmd
    """
    host = env["host"]
    set_env(host)
    path = create_sandbox()
    sfilepath = os.path.join(path, os.path.basename(dumpfile))
    try:
        local("cp {} {}".format(dumpfile, path))

        with lcd(path):
            local("tar xjvf {}".format(sfilepath))
        dumpdir = os.path.join(path, dumpfile.replace(".tar.bz2", ""))
        with lcd(dumpdir):
            local("mongorestore --db rentplatform {}".format("db/rentplatform"))
            local("rm -rf {}".format(os.path.join(env["app_dir"], "static/uploads")))
            local("mv {} {}".format("uploads", os.path.join(env["app_dir"], "static/")))
    finally:
        local("rm -rf {}".format(path))


@task
def import_database(server1, server2):
    """
        Import database from server1 to server2
    """
    # setting host environment of server1
    env.host_string  = server1
    set_env(server1)

    # creating name of unique directory
    dirname = time.time()
    path = "/tmp/{}".format(dirname)

    # creating unique directory
    run("mkdir -p {}".format(path))

    # creating name of directory with current date
    dump_dir_name =  "rentplatform_dump_" + datetime.datetime.strftime(datetime.datetime.today(), "%d_%m_%y")
    abs_dump_dir = os.path.join(path, dump_dir_name)
    db_dump_path = os.path.join(path, dump_dir_name, "db")

    try:
        # taking dump of instrta database at temporary location 
        run("mongodump --db rentplatform -o {dirname}".format(dirname = db_dump_path))

        # creating filename of compressed file 
        zipfilename = "{}.tar.bz2".format(dump_dir_name)

        # enter into temporary directory
        with cd(path):
            #create compreesed file
            run("tar cjf {filename} {dirname}".format(filename=zipfilename, dirname=dump_dir_name))

        zipfilepath = os.path.join(path, zipfilename)

        # setting host environment of server2
        env.host_string  = server2
        set_env(server2)

        # creating paths for location from where we copy file and to where we put file
        copyFrom =  env_settings[server1]['user'] + "@" + server1 +  ":" +zipfilepath
        copyTo = "/tmp/{}".format(time.time())
        sandboxpath = copyTo
        #creating temp directory to put file 
        run('mkdir -p {}'.format(copyTo))

        # copy compressed file from server1 to server2
        run("scp {copyFrom} {copyTo}".format(copyFrom=copyFrom,copyTo=copyTo))

        # clean existing database
        run("mongo rentplatform --eval 'db.dropDatabase()'")

        # enter in temp directory
        with cd(sandboxpath):
            # uncompress file
            run("tar xjf {}".format(zipfilename))

            # restore databse
            run("mongorestore --db rentplatform {dbdir}".format(dbdir=os.path.join(sandboxpath, dump_dir_name, "db/rentplatform") ))

    finally:
        print "cleaning the dir"
        # delete temp directory from server2
        run("rm -rf {}".format(sandboxpath))
        env.host_string  = server1
        set_env(server1)
        # delete temp directory from server1
        run("rm -rf {}".format(path))


@task
def import_media(server1, server2):
    """
        Import media folder from server1 to server2
    """

    # setting host environment of server1
    env.host_string  = server1
    set_env(server1)

    # creating name of unique directory
    path = "/tmp/{}".format(time.time())

    # creating name of directory with current date
    dump_dir_name =  "rentplatform_dump_" + datetime.datetime.strftime(datetime.datetime.today(), "%d_%m_%y")
    abs_dump_dir = os.path.join(path, dump_dir_name)

    try:
        # creating paths for location from where we copy uploads and to where we put uploads
        media_dump_path = os.path.join(path, dump_dir_name)
        app_media_path = os.path.join( env['app_dir'] , "static/uploads")

        # create temp directory where to put uploads folder
        run("mkdir -p {}".format(media_dump_path))

        # copy uploads from project_dir to temp dir
        run("cp -rf {frompath} {topath}".format(frompath=app_media_path, topath=media_dump_path))

        # creating filename of compressed file 
        zipfilename = "{}.tar.bz2".format(dump_dir_name)

        # enter in temp directory
        with cd(path):
            #create compreesed file
            run("tar cjf {filename} {dirname}".format(filename=zipfilename, dirname=dump_dir_name))

        zipfilepath = os.path.join(path, zipfilename)

        # setting host environment of server2
        env.host_string  = server2
        set_env(server2)

        # creating paths for location from where we copy file and to where we put file
        copyFrom =  env_settings[server1]['user'] + "@" + server1 +  ":" +zipfilepath
        copyTo = "/tmp/{}".format(time.time())

        #creating temp directory to put file 
        run('mkdir -p {}'.format(copyTo))

        # copy compressed file from server1 to server2
        run("scp {copyFrom} {copyTo}".format(copyFrom=copyFrom,copyTo=copyTo))

        sandboxpath = copyTo

        # enter in temp directory
        with cd(sandboxpath):
            # uncompress file
            run("tar xjf {}".format(zipfilename))
            path_to_uploads =  os.path.join(env_settings[server2]['app_dir'], "static/uploads")

            # delete existing uploads
            run("rm -rf {}".format(path_to_uploads))

            # copy new uploads
            run("cp -rf {fromdir} {todir}".format(fromdir=os.path.join(sandboxpath, dump_dir_name, "uploads"), todir=path_to_uploads))

    finally:
        print "cleaning the dir"
        # delete temp directory from server2
        run("rm -rf {}".format(sandboxpath))
        env.host_string  = server1
        set_env(server1)
        # delete temp directory from server1
        run("rm -rf {}".format(path))


@task
def import_data(server1, server2):
    """
        Import database and media folders from server1 to server2
    """
    import_database(server1, server2)
    import_media(server1, server2)


@task
def reload_mongo():
    local("rm /var/lib/mongodb/mongod.lock")
    local("mongod --repair")
    local("service mongodb start")
    local("service mongodb status")

