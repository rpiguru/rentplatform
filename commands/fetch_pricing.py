import sys
from flask.ext.script import Command

from serve import app
from models import ResidentialListing, Building
from feeds.yieldstar import YieldStarFeed
from feeds.ftp_price import FTPPriceFeed


class FetchPricingCommand(Command):
    def run(self):
        for building_name, property_id in app.config["FEEDS"]["YIELDSTAR"]["integrations"].items():
            try:
                building = Building.objects.filter(name__iexact=building_name).first()
                if not building:
                    continue
                units = ResidentialListing.objects.filter(
                    avail=True,
                    building=building.id,
                )
                feed = YieldStarFeed(property_id)
                for unit in units:
                    price = feed.get_price_for_unit(unit.apt_num)
                    if price:
                        unit.price = price
                        unit.save()
            except Exception as err:
                sys.stderr.write("Problem fetching yieldstar price for {}".format(building_name))
                sys.stderr.write("{}\n{}".format(err, err.__dict__))


class FTPPriceCommand(Command):
    def run(self):
        for building_name in app.config["FEEDS"]["FTPPriceFeed"]["integrations"].keys():
            try:
                building = Building.objects.filter(name__iexact=building_name).first()
                if not building:
                    continue
                units = ResidentialListing.objects.filter(
                    avail=True,
                    building=building.id,
                )
                feed = FTPPriceFeed()
                f, prices = feed.process(building_name)
                if prices:
                    for unit in units:
                        price = prices.get(unit.apt_num.zfill(6))
                        if price:
                            unit.price = price
                            unit.save()
                    feed.cleanup_file(f)
            except Exception as err:
                sys.stderr.write("Problem parsing price from ftp file for {}\n".format(building_name))
                sys.stderr.write("{}\n".format(err))

