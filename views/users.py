from flask import Blueprint, render_template, request, redirect
from flask.views import MethodView
from models import User
from flask.ext import login, wtf

users = Blueprint('users', __name__, template_folder="templates")


class LoginView(MethodView):
    methods = ['GET', 'POST']

    def dispatch_request(self):
        login.logout_user
        if login.current_user.is_anonymous():
            form = LoginForm(request.form)
            if form.validate_on_submit():
                user = form.get_user()
                login.login_user(user)
                return redirect('/admin/')
            else:
                print 'did not validate', str(form.errors)
            return render_template('users/login.html', form=form)
        else:
            return redirect('/')


class LoginForm(wtf.Form):
    login = wtf.TextField(validators=[wtf.validators.Required()])
    password = wtf.PasswordField(validators=[wtf.validators.Required()])

    def validate_login(self, field):
        user = self.get_user()
        if user is None:
            raise wtf.ValidationError('Invalid login')

        if user.password != self.password.data:
            raise wtf.ValidationError('Invalid password')

    def get_user(self):
        user = User.objects(login=self.login.data).first()
        return user


class LogoutView(MethodView):
    methods = ['GET', 'POST']

    @login.login_required
    def dispatch_request(self):
        login.logout_user()

        return redirect('/login/')


users.add_url_rule('/login/', view_func=LoginView.as_view('login'))
users.add_url_rule('/logout/', view_func=LogoutView.as_view('logout'))
