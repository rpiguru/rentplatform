import datetime
import json
import os
import time
from urllib import urlencode, quote_plus
from urlparse import urlparse

from PIL import Image
from bson.objectid import ObjectId
from flask import Blueprint, flash, render_template, redirect, request, url_for, \
    jsonify
from flask.ext import login, wtf
from flask.ext.mongoengine.wtf.fields import ModelSelectField
from flask.views import MethodView
from wtforms.validators import ValidationError

from models import Building, Extra, Neighborhood, Borough, User, \
    Gallery, Photos, ResidentialListing, UnitFeature, \
    BuildingType, Activity, View
from serve import app, buildinglogos, buildingbgs, \
    buildingsolo, floorplans
from utils import clone_listing, clone_floorplan, clone_gallery


def checkImageFileType(self, field):
    """
    Validate Image Type
    """
    if request.files[field.id]:
        filename = request.files[field.id]
        allowed_extensions = set(['image/jpeg', 'image/png', 'image.jpg', 'image/gif'])
        if not(filename.content_type in allowed_extensions):
            raise ValidationError('Wrong Filetype, you can upload only png,jpg,jpeg,gif files')


managers = Blueprint('managers', __name__, template_folder="templates")


def before_managers():
    if login.current_user.is_anonymous():
        return redirect('/login/')

managers.before_request(before_managers)


class ManagerBuildings(MethodView):
    def get(self, action=None, building_id=None, photo_index=0):
        neighborhoods = Neighborhood.objects.order_by('name')
        b = []
        if not action:
            for builds in Building.objects().order_by('name'):
                b.append({'bui':builds})
            return render_template('manager/admin_buildings.html',
                buildings=b,
                boroughs=sorted(Borough.objects(), key=lambda x: x.name),
                unit_features=sorted(UnitFeature.objects(), key=lambda x: x.name),
                features=sorted(Extra.objects(), key=lambda x: x.name),
                views=sorted(View.objects(), key=lambda x: x.name),
                neighborhoods=neighborhoods)
        elif action == 'new':
            form = ManagerBuildingsForm(request.form)
            form.extras.choices = [(e.id, e.name) for e in Extra.objects.order_by('name')]
            form.unit_features.choices = [(uf.id, uf.name) for uf in UnitFeature.objects.order_by('name')]
            form.neighborhood.queryset = Neighborhood.objects.order_by('name')
            form.building_type.queryset = BuildingType.objects.order_by('name')
            return render_template('manager/admin_buildings_new.html',
                building=None,
                form=form,
                action=action,
                neighborhoods=neighborhoods)
        elif action == 'delete':
            building = Building.objects(id=building_id).first()
            residentials_for_building = ResidentialListing.objects(building=building.id)
            residentials_for_building.delete()
            building.delete()
            redirect('/admin/buildings/')
        elif action == 'edit':
            building = Building.objects(id=building_id).first()
            form = ManagerBuildingsForm(request.form, obj=building)
            form.extras.choices = [(e.id, e.name) for e in Extra.objects.order_by('name')]
            form.unit_features.choices = [(uf.id, uf.name) for uf in UnitFeature.objects.order_by('name')]
            form.neighborhood.queryset = Neighborhood.objects.order_by('name')
            form.building_type.queryset = BuildingType.objects.order_by('name')
            return render_template('manager/admin_buildings_new.html',
                building=building,
                form=form,
                action=action,
                neighborhoods=neighborhoods)
        else:
            # SHOULD NOT GET HERE
            return render_template('manager/admin_buildings.html',
                boroughs=sorted(Borough.objects(), key=lambda x: x.name),
                buildings=b,
                features=Extra.objects(),
                neighborhoods=neighborhoods)

    def post(self, action=None, building_id=None):
        neighborhoods = Neighborhood.objects.order_by('name')
        form = ManagerBuildingsForm(request.form)
        form.extras.choices = [(e.id, e.name) for e in Extra.objects.order_by('name')]
        form.unit_features.choices = [(uf.id, uf.name) for uf in UnitFeature.objects.order_by('name')]
        form.neighborhood.queryset = Neighborhood.objects.order_by('name')
        form.building_type.queryset = BuildingType.objects.order_by('name')

        has_pics = False
        if building_id:
            building = Building.objects(id=building_id).first()
            if action == 'delete':
                pass
        else:
            building = Building()
        if form.validate():

            if 'logo' in request.files:
                pic = request.files['logo']
                if pic.filename:
                    building.logo = buildinglogos.save(request.files['logo'])

            if 'bg_img_main' in request.files:
                pic = request.files['bg_img_main']
                if pic.filename:
                    building.bg_img_main = buildingbgs.save(request.files['bg_img_main'])

            if 'bg_img_solo' in request.files:
                pic = request.files['bg_img_solo']
                if pic.filename:
                    building.bg_img_solo = buildingsolo.save(request.files['bg_img_solo'])

            building.name = form.name.data
            building.building_type = form.building_type.data
            building.address = form.address.data
            building.city = form.city.data
            building.state = form.state.data
            building.zip_code = form.zip_code.data
            building.neighborhood = form.neighborhood.data
            building.borough = building.neighborhood.borough
            building.agent_name = form.agent_name.data
            building.agent_phone = form.agent_phone.data
            building.agent_email = form.agent_email.data
            building.lat = form.lat.data
            building.lng = form.lng.data
            building.extras = form.extras.data
            building.unit_features = form.unit_features.data
            building.year = form.year.data
            building.description = form.description.data
            building.floors = form.floors.data
            building.units = form.units.data
            building.entity = form.entity.data
            building.rentals_id = form.rentals_id.data
            building.link = form.link.data

            building.save()
            return redirect('/admin/buildings/')

        else:
            print form.errors
            return render_template('manager/admin_buildings_new.html',
                building=building,
                form=form,
                action=action,
                neighborhoods=neighborhoods)


class ManagerBuildingsForm(wtf.Form):
    name = wtf.TextField("Building Name", validators=[wtf.validators.Required()])
    entity = wtf.TextField("Entity", validators=[wtf.validators.Required()])
    rentals_id = wtf.TextField("Rentals.com ID", validators=[wtf.validators.Optional()])
    neighborhood = ModelSelectField("Neighborhood", model=Neighborhood)
    building_type = ModelSelectField("Building Type", model=BuildingType)
    address = wtf.TextField(validators=[wtf.validators.Required()])
    city = wtf.TextField('City', validators=[wtf.validators.Required()])
    state = wtf.TextField('State', validators=[wtf.validators.Required()])
    zip_code = wtf.TextField('Zip', validators=[wtf.validators.Required()])
    lat = wtf.TextField('Latitude')
    lng = wtf.TextField('Longitude')
    agent_name = wtf.TextField('Agent Name', validators=[wtf.validators.Required()])
    agent_phone = wtf.TextField('Agent Phone #', validators=[wtf.validators.Required()])
    agent_email = wtf.TextField('Agent Email', validators=[wtf.validators.Required()])
    description = wtf.TextAreaField('Description', validators=[wtf.validators.Required()])
    extras = wtf.SelectMultipleField("Building Features", coerce=ObjectId, choices=[(e.id, e.name) for e in Extra.objects.order_by('name')])
    unit_features = wtf.SelectMultipleField("Unit Features", coerce=ObjectId, choices=[(e.id, e.name) for e in UnitFeature.objects.order_by('name')])
    year = wtf.TextField("Year built")
    floors = wtf.IntegerField("# of Floors")
    units = wtf.IntegerField("# of Units")
    photos = wtf.FileField('Photos')
    link = wtf.TextField('Property Website URL', description='http://your_property_url or https://your_property_url', validators=[wtf.validators.Optional(), wtf.validators.regexp(u'^http+s?://', message=u'URL format should be - http://your_property_url or https://your_property_url')])


class ManagerBuildingFeatures(MethodView):
    def get(self, action=None, building_feature_id=None):
        if not action:
            return redirect('/admin/buildings/')

        elif action == 'new':
            form = ManagerBuildingFeaturesForm(request.form)
            return render_template('manager/admin_building_features_new.html',
                feature=None,
                form=form,
                action=action)
        elif action == 'edit':
            building_feature = Extra.objects(id=building_feature_id).first()
            building_feature.name = building_feature.name
            form = ManagerBuildingFeaturesForm(request.form, obj=building_feature)
            return render_template('manager/admin_building_features_new.html',
                feature=building_feature,
                form=form,
                action=action)
        elif action == 'delete':
            building_feature = Extra.objects(id=building_feature_id).first()
            building_feature.delete()
            return redirect('/admin/buildings/')

    def post(self, action=None, building_feature_id=None):
        form = ManagerBuildingFeaturesForm(request.form)
        feature = None
        if action == 'delete':
            if building_feature_id:
                feature = Extra.objects(id=building_feature_id).first()
                feature.delete()
                return 'ok'
        elif form.validate():
            if building_feature_id:
                feature = Extra.objects(id=building_feature_id).first()
            else:
                feature = Extra()

            feature.name = form.name.data
            feature.street_easy = form.street_easy.data
            feature.street_easy_other = form.street_easy_other.data
            feature.rentals_parent = form.rentals_parent.data
            feature.rentals = form.rentals.data
            feature.rentals_value = form.rentals_value.data
            feature.save()
            return redirect('/admin/buildings/')
        else:
            return render_template('manager/admin_building_features_new.html',
                feature=feature,
                form=form,
                action=action)


class ManagerBuildingFeaturesForm(wtf.Form):
    name = wtf.TextField('Name')
    street_easy = wtf.TextField('Street Easy Tag')
    street_easy_other = wtf.BooleanField('Is Street Easy Other Amenity')
    rentals_parent = wtf.TextField('Rentals.com Parent Tag')
    rentals = wtf.TextField('Rentals.com Tag')
    rentals_value = wtf.TextField('Rentals.com Value')


class ManagerUnitFeatures(MethodView):
    def get(self, action=None, unit_feature_id=None):
        if not action:
            return redirect('/admin/buildings/')

        elif action == 'new':
            form = ManagerUnitFeaturesForm(request.form)
            return render_template('manager/admin_unit_features_new.html',
                feature=None,
                form=form,
                action=action)
        elif action == 'edit':
            unit_feature = UnitFeature.objects(id=unit_feature_id).first()
            form = ManagerUnitFeaturesForm(request.form, obj=unit_feature)
            return render_template('manager/admin_unit_features_new.html',
                feature=unit_feature,
                form=form,
                action=action)
        elif action == 'delete':
            unit_feature = UnitFeature.objects(id=unit_feature_id).first()
            unit_feature.delete()
            return redirect('/admin/buildings/')

    def post(self, action=None, unit_feature_id=None):
        form = ManagerUnitFeaturesForm(request.form)
        feature = None
        if action == 'delete':
            if unit_feature_id:
                feature = UnitFeature.objects(id=unit_feature_id).first()
                feature.delete()
                return 'ok'
        elif form.validate():
            if unit_feature_id:
                feature = UnitFeature.objects(id=unit_feature_id).first()
            else:
                feature = UnitFeature()

            feature.name = form.name.data
            feature.street_easy = form.street_easy.data
            feature.street_easy_other = form.street_easy_other.data
            feature.rentals_parent = form.rentals_parent.data
            feature.rentals = form.rentals.data
            feature.rentals_value = form.rentals_value.data
            feature.save()
            return redirect('/admin/buildings/')
        else:
            return render_template('manager/admin_unit_features_new.html',
                feature=feature,
                form=form,
                action=action)


class ManagerUnitFeaturesForm(wtf.Form):
    name = wtf.TextField('Name')
    street_easy = wtf.TextField('Street Easy Tag')
    street_easy_other = wtf.BooleanField('Is Street Easy Other Amenity')
    rentals_parent = wtf.TextField('Rentals.com Parent Tag')
    rentals = wtf.TextField('Rentals.com Tag')
    rentals_value = wtf.TextField('Rentals.com Value')


class ManagerViews(MethodView):
    def get(self, action=None, view_id=None):
        if not action:
            return redirect('/admin/buildings/')

        elif action == 'new':
            form = ManagerUnitFeaturesForm(request.form)
            return render_template('manager/admin_views_new.html',
                view=None,
                form=form,
                action=action)
        elif action == 'edit':
            view = View.objects(id=view_id).first()
            form = ManagerViewsForm(request.form, obj=view)
            return render_template('manager/admin_views_new.html',
                view=view,
                form=form,
                action=action)
        elif action == 'delete':
            view = UnitFeature.objects(id=view_id).first()
            view.delete()
            return redirect('/admin/buildings/')

    def post(self, action=None, view_id=None):
        form = ManagerViewsForm(request.form)
        view = None
        if action == 'delete':
            if view_id:
                view = View.objects(id=view_id).first()
                view.delete()
                return 'ok'
        elif form.validate():
            if view_id:
                view = View.objects(id=view_id).first()
            else:
                view = View()

            view.name = form.name.data
            view.save()
            return redirect('/admin/buildings/')
        else:
            return render_template('manager/admin_views_new.html',
                view=view,
                form=form,
                action=action)


class ManagerViewsForm(wtf.Form):
    name = wtf.TextField('Name')


class ManagerBorough(MethodView):
    def get(self, action=None, borough_id=None):
        if not action:
            return redirect('/admin/')

        if action == 'new':
            form = ManagerBoroughsForm(request.form)
            return render_template('manager/admin_boroughs_new.html',
                borough=None,
                form=form,
                action=action)

        if action == 'edit':
            borough = Borough.objects(id=borough_id).first()
            borough.name = borough.name
            form = ManagerBoroughsForm(request.form, obj=borough)
            return render_template('manager/admin_boroughs_new.html',
                borough=borough,
                form=form,
                action=action)

        if action == 'delete':
            if borough_id:
                borough = Borough.objects(id=borough_id).first()
                borough.delete()
                return redirect('/admin/buildings/')

    def post(self, action=None, borough_id=None):
        borough = None
        if login.current_user.is_anonymous():
            return redirect('/login/')

        form = ManagerBoroughsForm(request.form)
        if action == 'delete':
            if borough_id:
                borough = Borough.objects(id=borough_id).first()
                borough.delete()
                return 'ok'
        elif form.validate():
            if borough_id:
                borough = Borough.objects(id=borough_id).first()
            else:
                borough = Borough()

            borough.name = form.name.data
            borough.save()
            return redirect('/admin/buildings/')
        else:
            print form.errors
            return render_template('manager/admin_boroughs_new.html',
                borough=borough,
                form=form,
                action=action)


class ManagerNeighborhood(MethodView):
    def get(self, action=None, neighborhood_id=None):
        if not action:
            return render_template('manager/admin_neighborhoods.html',
                neighborhoods=Neighborhood.objects.order_by('name'))

        if action == 'new':
            form = ManagerNeighborhoodsForm(request.form)
            form.borough.queryset= Borough.objects.order_by('name')
            return render_template('manager/admin_neighborhoods_new.html',
                neighborhood=None,
                form=form,
                action=action)

        if action == 'edit':
            neighborhood = Neighborhood.objects(id=neighborhood_id).first()
            neighborhood.name = neighborhood.name
            form = ManagerNeighborhoodsForm(request.form, obj=neighborhood)
            form.borough.queryset= Borough.objects.order_by('name')
            return render_template('manager/admin_neighborhoods_new.html',
                neighborhood=neighborhood,
                form=form,
                action=action,)

        if action == 'delete':
            neighborhood = Neighborhood.objects(id=neighborhood_id).first()
            neighborhood.delete()
            return redirect('/admin/neighborhoods/')

        elif action == 'crop':
            neighborhood = Neighborhood.objects(id=neighborhood_id).first()
            return render_template('manager/admin_crop_neighborhood_photos.html',
            neighborhood=neighborhood)

    def post(self, action=None, neighborhood_id=None):
        form = ManagerNeighborhoodsForm(request.form)
        form.borough.queryset= Borough.objects.order_by('name')
        if neighborhood_id:
            neighborhood = Neighborhood.objects(id=neighborhood_id).first()
        else:
            neighborhood = None
        if form.validate():
            if neighborhood_id:
                neighborhood = Neighborhood.objects(id=neighborhood_id).first()
            else:
                neighborhood = Neighborhood()

            neighborhood.name = form.name.data
            neighborhood.desc = form.desc.data
            neighborhood.borough = form.borough.data
            neighborhood.save()
            return redirect('/admin/neighborhoods/')
        else:
            return render_template('manager/admin_neighborhoods_new.html',
                form=form,
                neighborhood=neighborhood,
                action=action)


class ManagerNeighborhoodsForm(wtf.Form):
    name = wtf.TextField('Name', validators=[wtf.validators.Required()])
    desc = wtf.TextAreaField('Description')
    borough = ModelSelectField('Borough', model=Borough)


class ManagerBoroughsForm(wtf.Form):
    name = wtf.TextField('Name')


class ManagerUsers(MethodView):
    def get(self, action=None, user_id=None):
        # INDEX
        if login.current_user.is_anonymous():
            return redirect('/login/')
        if not login.current_user.is_admin and not login.current_user.is_super_user:
            return redirect('/')
        if not action:
            users = User.objects.all()
            return render_template('manager/admin_users.html',
                users=users)
        # NEW
        if action == 'new':
            form = ManagerUsersForm(request.form)
            return render_template('manager/admin_users_new.html', form=form, user=None, action=action, placeholder="")
        # EDIT
        if action == 'edit':
            user = User.objects(id=user_id).first()
            if user.is_super_user and not login.current_user.is_super_user:
                return redirect('/admin/users/')
            form = ManagerUsersForm(request.form, obj=user, action=action)
            return render_template('manager/admin_users_new.html',
                form=form, user=user,
                action=action,
                placeholder="Leave blank for no change")

        # DELETE
        if action == 'delete':
            user = User.objects(id=user_id).first()
            if user.is_super_user and not login.current_user.is_super_user:
                return redirect('/admin/users/')
            user.delete()
            return redirect('/admin/users/')

    def post(self, action=None, user_id=None):
        if not login.current_user.is_admin and not login.current_user.is_super_user:
            return redirect('/')
        form = ManagerUsersForm(request.form)

        user = User()
        if action == 'edit':
            user = User.objects(id=user_id).first()
            if user.is_super_user and not login.current_user.is_super_user:
                return redirect('/admin/users/')
            if user.id != login.current_user.id:
                del form.password
            else:
                form.password.validators = [wtf.validators.EqualTo('confirm_password', message='Passwords must match')]

        if form.validate():
            if action == 'new':
                user.password = form.password.data
            elif user.id == login.current_user.id and form.password.data:
                user.password = form.password.data
            user.login = form.login.data
            user.email = form.email.data
            user.company = form.company.data
            user.tele = form.tele.data
            user.name = form.name.data
            user.is_admin = form.is_admin.data
            if login.current_user.is_super_user:
                user.is_super_user = form.is_super_user.data

            user.save()
            return redirect('/admin/users/')
        else:
            print form.errors

        return render_template('manager/admin_users_new.html', form=form, user=user, action=action, placeholder="")


class ManagerUsersForm(wtf.Form):
    login = wtf.TextField(validators=[wtf.validators.Length(min=4, max=80)])
    email = wtf.StringField(validators=[wtf.validators.Length(min=6, max=120),
        wtf.validators.Email(message='Emails must be valid')])
    password = wtf.PasswordField(validators=[
        wtf.validators.Required(),
        wtf.validators.EqualTo('confirm_password', message='Passwords must match')
        ])
    confirm_password = wtf.PasswordField()
    company = wtf.StringField()
    tele = wtf.StringField('Phone #')
    name = wtf.StringField()
    is_admin = wtf.BooleanField('Is Admin')
    is_super_user = wtf.BooleanField('Is Super User')


class ManagerBuildingsDeletePic(MethodView):
    def post(self, building_id, pic):
        import os
        building = Building.objects(id=building_id).first()
        pics = building.photos
        if pic in building.photos:
            building.photos.remove(pic)
            building.save()
            try:
                os.remove('/%s/%s' % (app.config["UPLOADED_BUILDINGPHOTOS_DEST"], pic))
                os.remove('/%s/big/%s' % (app.config["UPLOADED_BUILDINGPHOTOS_DEST"], pic))
                os.remove('/%s/med/%s' % (app.config["UPLOADED_BUILDINGPHOTOS_DEST"], pic))
                os.remove('/%s/small/%s' % (app.config["UPLOADED_BUILDINGPHOTOS_DEST"], pic))
            except OSError:
                pass
        return 'success'


class ManagerCropImage(MethodView):
    """
    To crop the images 
    """
    def post(self, model_type, model_id, photo_index, skip=None):
        w = int(request.form.get('w', 0))
        h = int(request.form.get('h', 0))
        x = int(request.form.get('x', 0))
        y = int(request.form.get('y', 0))
        photo_dir = ''
        photos = []
        photo = ''

        if model_type == 'buildings':
            photo_dir = app.config["UPLOADED_BUILDINGPHOTOS_DEST"]
            photos = Building.objects(id=model_id).first().photos
        elif model_type == 'residentials':
            photo_dir = app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"]
            photos = ResidentialListing.objects(id=model_id).first().photos


        photo = photos[int(photo_index)]
        imageFile = '%s/%s' % (photo_dir, photo)
        if not skip:
            # have added none check, as 0 is a valid value and should be allowed
            if (x or x == 0) and (y or y == 0) and (w or w == 0) and (h or h == 0):
                img = Image.open(imageFile)
                # left, upper, right, lower
                box = (x, y, x + w, y + h)
                if any(box):
                    try:
                        region = img.crop(box)
                        region.save('%s/%s' % (photo_dir, photo))
                    except SystemError :
                        print "error while cropping the image"

            sizes = {"small":(120, 65), "med":(300, 165), "big":(800, 435)}

            for key, val in sizes.iteritems():
                size = val
                img = Image.open(imageFile)
                img.thumbnail(size, Image.ANTIALIAS)
                if not os.path.exists('%s/%s' % (photo_dir, key)):
                    os.makedirs('%s/%s' % (photo_dir, key))
                img.save('%s/%s/%s' % (photo_dir, key , photo))

        return 'OK'


class BuildingPhotos(MethodView):
    """
    API to fetch photos for buildings
    """

    def get(self, building_id):
        building = Building.objects(id=building_id).first()
        photo_info = []
        search = request.args['q'];
        if building.gallery:
            for photo in building.gallery.photos:
                if photo.name:
                    if search in photo.name:
                        photo_info.append({"id": str(photo.id), "text": photo.name, "thumbnail": photo.get_thumb })
                else:
                    if search in photo.path.split('/')[1]:
                        photo_info.append({"id": str(photo.id), "text": photo.path.split('/')[1], "thumbnail": photo.get_thumb })
    
        result = { "photos" : photo_info }
        return jsonify(result)


class ManagerResidential(MethodView):
    def get(self, action=None, residential_id=None, photo_index=0):
        if login.current_user.is_anonymous():
            return redirect('/login/')
        l = []
        if not action:

            listings = ResidentialListing.objects

            # for listing in listings.order_by('-avail'):
            for listing in listings:

                building = listing.get_building()
                if building is None:
                    building = Building(address="none", neighborhood="woodside")

                if not building:
                    listing.delete()
                    return redirect("/admin/residentials")
                l.append({'listing' : listing,
                    'building' : building})


                """Sort According To Availability Neighbourhood and Address"""
                l = sorted(l, key=lambda lst: lst['building']['address'])
                l = sorted(l, key=lambda lst: lst['building']['neighborhood'])
                l = sorted(l, key=lambda lst: lst['listing']['avail'], reverse=True)
            return render_template('manager/admin_residential.html', listings_collection=l, features=UnitFeature.objects())
        elif action == 'new':
            form = ManagerResidentialForm(request.form)
            form.building.choices = [(b.id, b.address) for b in Building.objects.order_by('name')]
            form.specs.choices = [(e.id, e.name) for e in UnitFeature.objects.order_by('name')]
            form.views.choices = [(e.id, e.name) for e in View.objects.order_by('name')]
            return render_template('manager/admin_residential_new.html', form=form, action=action, residential=None)
        elif action == 'edit':
            residential = ResidentialListing.objects(id=residential_id).first()
            form = ManagerResidentialForm(request.form, obj=residential)
            form.building.choices = [(b.id, b.address) for b in Building.objects.order_by('name')]
            form.specs.choices = [(e.id, e.name) for e in UnitFeature.objects.order_by('name')]
            form.views.choices = [(e.id, e.name) for e in View.objects.order_by('name')]
            return render_template('manager/admin_residential_new.html', form=form, action=action, residential=residential)
        elif action == 'clone':
            original = ResidentialListing.objects(id=residential_id).first()
            residential = clone_listing(original)
            # Fake these just to get the form to render correctly.
            residential.id = original.id
            residential.floorplan = original.floorplan
            residential.floorplan_pdf = original.floorplan_pdf
            residential.apt_num = None
            form = ManagerResidentialForm(request.form, obj=residential)
            form.building.choices = [(b.id, b.address) for b in Building.objects.order_by('name')]
            form.specs.choices = [(e.id, e.name) for e in UnitFeature.objects.order_by('name')]
            form.views.choices = [(e.id, e.name) for e in View.objects.order_by('name')]
            return render_template('manager/admin_residential_new.html', form=form, action=action, residential=residential, original=original)
        elif action == 'delete':
            residential = ResidentialListing.objects(id=residential_id).first()
            building = Building.objects(id=residential.building).first()

            did = "Deleted"
            residential.delete()
            return redirect("/admin/residentials/")

        else:
            return redirect("/admin/residentials/")

    def post(self, action=None, residential_id=None):
        form = ManagerResidentialForm(request.form)
        form.building.choices = [(b.id, b.address) for b in Building.objects.order_by('name')]
        form.specs.choices = [(e.id, e.name) for e in UnitFeature.objects.order_by('name')]
        form.views.choices = [(e.id, e.name) for e in View.objects.order_by('name')]
        
        residential = ResidentialListing.objects.get_or_404(id=residential_id) if residential_id else None
        
        if form.validate(residential):
            residential = ''
            original_residential = None
            if action in ['new', 'clone']:
                residential = ResidentialListing()
                already_exists = ResidentialListing.objects(building=form.building.data, apt_num=form.apt_num.data).first()
                if already_exists:
                    return redirect('/admin/residentials/')
            if action == 'clone':
                original_residential = ResidentialListing.objects(id=residential_id).first()
            if action == 'edit':
                residential = ResidentialListing.objects(id=residential_id).first()

            if form.remove_floorplan.data:
                residential.floorplan = None
            elif 'floorplan' in request.files and request.files['floorplan'].filename != '':
                residential.floorplan = floorplans.save(request.files['floorplan'])
            elif action == 'clone' and original_residential and original_residential.floorplan:
                residential.floorplan = clone_floorplan(original_residential)

            if form.remove_floorplan_pdf.data:
                residential.floorplan_pdf = None
            elif 'floorplan_pdf' in request.files and request.files['floorplan_pdf'].filename != '':
                residential.floorplan_pdf = floorplans.save(request.files['floorplan_pdf'])
            elif action == 'clone' and original_residential and original_residential.floorplan_pdf:
                residential.floorplan_pdf = clone_floorplan(original_residential)

            residential.building = form.building.data
            residential.specs = form.specs.data
            residential.views = form.views.data
            residential.floor = form.floor.data
            residential.apt_num = form.apt_num.data
            residential.bed = form.bed.data
            residential.bath = form.bath.data
            residential.description = form.description.data
            residential.sqft = form.sqft.data
            residential.avail = form.avail.data
            residential.owner_pays = form.owner_pays.data
            residential.price = form.price.data
            residential.url = form.url.data

            date = datetime.datetime.strptime(request.form["availability"], "%m/%d/%Y")
            if date:
                residential.availability = date

            residential.modified_date = datetime.datetime.strptime(time.strftime("%m/%d/%Y"), "%m/%d/%Y")

            residential.save()

            if action == 'clone' and original_residential and original_residential.photo_gallery:
                # Update the gallery after the listing has been saved.
                residential.photo_gallery = clone_gallery(original_residential.photo_gallery)
                residential.save()

            return redirect('/admin/residentials/')

        if action == 'new':
            return render_template("manager/admin_residential_new.html", form=form, residential=None, action=action)
        if action == 'edit':
            residential = ResidentialListing.objects(id=residential_id).first()
            return render_template("manager/admin_residential_new.html", form=form, residential=residential, action=action)
        if action == 'clone':
            original = ResidentialListing.objects(id=residential_id).first()
            residential = clone_listing(original)
            # Fake these just to get the form to render correctly.
            residential.id = original.id
            residential.floorplan = original.floorplan
            residential.floorplan_pdf = original.floorplan_pdf
            return render_template('manager/admin_residential_new.html', form=form, action=action, residential=residential)

    def fix_photos(self, current_residential):
        sizes = {"small":60, "med":321, "big":388}
        currDir = app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"]
        for pic in current_residential.photos:
            imageFile = '%s/%s' % (app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"], pic)
 
            for key in sizes:
                size = sizes[key], sizes[key]
                img = Image.open(imageFile)
                img.thumbnail(size, Image.ANTIALIAS)
                if not os.path.exists('%s/%s' % (currDir, key)):
                    os.makedirs('%s/%s' % (currDir, key))
                img.save('%s/%s/%s' % (currDir, key , pic))


class ManagerResidentialsDeletePic(MethodView):
    def post(self, residential_id, pic):
        import os
        residential = ResidentialListing.objects(id=residential_id).first()
        pics = residential.photos
        if pic in residential.photos:
            residential.photos.remove(pic)
            residential.save()
            try:
                os.remove('/%s/%s' % (app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"], pic))
                os.remove('/%s/big/%s' % (app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"], pic))
                os.remove('/%s/med/%s' % (app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"], pic))
                os.remove('/%s/small/%s' % (app.config["UPLOADED_RESIDENTIALPHOTOS_DEST"], pic))
            except OSError:
                pass
        return 'success'


class ManagerResidentialForm(wtf.Form):
    building = wtf.SelectField("Building", coerce=ObjectId, choices=[(b.id, b.address) for b in Building.objects.order_by('name')])
    floor = wtf.IntegerField(validators=[wtf.validators.NumberRange(min=0, max=1000)])
    apt_num = wtf.TextField('Unit', validators=[wtf.validators.Required()])
    bed = wtf.IntegerField(validators=[wtf.validators.NumberRange(min=0, max=10)])
    bath = wtf.FloatField(validators=[wtf.validators.NumberRange(min=0, max=10)])
    photos = wtf.FileField('Photos')
    sqft = wtf.IntegerField('Square Feet', validators=[wtf.validators.optional(), wtf.validators.NumberRange(min=0, max=100000)])
    description = wtf.TextAreaField('Description', validators=[wtf.validators.Required()])
    floorplan = wtf.FileField('Floor Plan')
    remove_floorplan = wtf.BooleanField('Remove Floor Plan', validators=[wtf.validators.Optional()])
    floorplan_pdf = wtf.FileField('Floor Plan PDF')
    remove_floorplan_pdf = wtf.BooleanField('Remove PDF', validators=[wtf.validators.Optional()])
    price = wtf.IntegerField(validators=[wtf.validators.NumberRange(min=0, max=999999999999999999)])
    owner_pays = wtf.BooleanField('Owner Pays')
    specs = wtf.SelectMultipleField("Features", coerce=ObjectId, choices=[(e.id, e.name) for e in UnitFeature.objects.order_by('name')])
    views = wtf.SelectMultipleField("Views", coerce=ObjectId, choices=[(e.id, e.name) for e in View.objects.order_by('name')])
    avail = wtf.BooleanField('Available')
    availability = wtf.DateTimeField('Availability Date', format='%m/%d/%Y', validators=[wtf.validators.Required()])
    url = wtf.TextField('Listing URL', validators=[wtf.validators.Required()])

    def validate(self, instance):
        """
        extra validation for apt_num uniqueness on building
        """
        if not wtf.Form.validate(self):
            return False
        building_units = ResidentialListing.objects.filter(building=self.building.data)
        if instance:
            building_units = building_units.filter(id__ne=instance.id)
             
        if building_units.filter(apt_num=self.apt_num.data):
            self.apt_num.errors.append("Unit with given value already exists.")
            return False
        else:
            return True


class ManagerResidentialAvailable(MethodView):
    def get(self, available_id, available):
        if login.current_user.is_anonymous():
            return redirect('/login/')
        residential = ResidentialListing.objects(id=available_id).first()
        residential.avail = bool(int(available))
        residential.save()
        return json.dumps({ 'result' : 'success' })


class ManagerListUnits(MethodView):
    def get(self, building_id):

        units = ResidentialListing.objects(building=building_id)
        json_units = []
        json_unit_photos = []

        for unit in units:
            json_units.append({
                "text": unit.apt_num,
                "id": str(unit.id)
                })
            
            if unit.photo_gallery:
                for photo in unit.photo_gallery.photos:
                    json_unit_photos.append({"text": photo.name, "thumbnail": photo.get_thumb})
        result = { "units" : json_units, "photos" : json_unit_photos}
        return jsonify(result)


class ManagerListUnitPhotos(MethodView):
    """
    View for populating unit photos in select2
    """
    def get(self, unit_id):
        unit = ResidentialListing.objects(id=unit_id).first()
        json_unit_photos = []
        search = request.args['q'];

        for photo in unit.photo_gallery.photos:
            if photo.name:
                if search in photo.name:
                    json_unit_photos.append({"id": str(photo.id), "text": photo.name, "thumbnail": photo.get_thumb })
            else:
                if search in photo.path.split('/')[1]:
                    json_unit_photos.append({"id": str(photo.id), "text": photo.path.split('/')[1], "thumbnail": photo.get_thumb })

        result = {"photos" : json_unit_photos}
        return jsonify(result)


class ManagerJSONBuildings(MethodView):
    def get(self):
        buildings = Building.objects.all()
        json_buildings = []
        for building in buildings:
            json_buildings.append({"text": building.address, "id" : str(building.id)})

        result = { "buildings" : json_buildings }
        return jsonify(result)


class ManagerGalleryForm(wtf.Form):
    """
    Form to upload image in existing gallery.
    """
    photos = wtf.FileField('Add Photo')


class ManagerBuildingGallery(MethodView):
    """
    Redirect to gallery page.
    """
    def get(self, building_id):
        """
        Redirect to gallery page with building gallery.
        """
        if login.current_user.is_anonymous():
            return redirect('/login/')
        building = Building.objects.get(id=building_id)

        if not building.gallery:
            gallery = Gallery()
            building.gallery = gallery
            gallery.save()
            building.save()

        target_url = urlparse(request.referrer)
        return redirect(url_for('manage_gallery', gallery_id=building.gallery.id) + '?' + urlencode({'target':target_url.path}))


class ManagerListingGallery(MethodView):
    """
    Redirect to gallery page.
    """
    def get(self, listing_id):
        """
        Redirect to gallery page with listing gallery.
        """
        if login.current_user.is_anonymous():
            return redirect('/login/')
        listing = ResidentialListing.objects.get(id=listing_id)

        if not listing.photo_gallery:
            gallery = Gallery()
            listing.photo_gallery = gallery
            gallery.save()
            listing.save()

        target_url = urlparse(request.referrer)
        return redirect(url_for('manage_gallery', gallery_id=listing.photo_gallery.id) + '?' + urlencode({'target':target_url.path}))


class ManagerGallery(MethodView):
    """
    Display photos of gallery with delete functionality.
    """
    def get(self, gallery_id, action=None, photo_id=None):
        """
        Redirect to gallery page.
        """
        if login.current_user.is_anonymous():
            return redirect('/login/')
        if action == "delete":
            return self.delete_photo(gallery_id, photo_id)
        else:
            gallery = Gallery.objects.get(id=gallery_id)
            photos = self.get_photos(gallery)

            target_url = ''
            if request.args.get('target') is not None:
                target_url = request.args.get('target')

            next_target_encoded = quote_plus(request.path + '?target=' + quote_plus(target_url))

            timestamp = int(time.time())
            return render_template('manager/admin_gallery.html',
                                    gallery=gallery,
                                    photos=photos,
                                    target=target_url,
                                    target_encoded=next_target_encoded,
                                    timestamp=timestamp
                                    )

    def get_photos(self, gallery):
        """
        Returns list of photos which are not in gallery.
        """
        photo_list = []
        photos = Photos.objects.all()
        for photo in photos:
            if photo not in gallery.photos:
                photo_list.append(photo)
        return photo_list


    def delete_photo(self, gallery_id, photo_id):
        """
        Delete photo from gallery database & from file system.
        """
        try:
            photo = Photos.objects.get(id=photo_id)
            gallery = Gallery.objects.get(id=gallery_id)
            if photo:
                gallery.photos.remove(photo)
                gallery.save()
        except:
            flash('Photo does not exist.', 'error')
        flash('Photo deleted successfully from gallery.', 'success')
        return redirect(url_for('manage_gallery', gallery_id=gallery_id))


class ManagerPhotosForm(wtf.Form):
    """
    Form to add new photo record.
    """
    def validate_name(form, field):
        """
        Duplicate value (case insensitive) validator for name field
        """
        name = field.data.strip()
        if name != '':
            photo = Photos.objects.filter(id__ne=form.id.data).filter(name__iexact=name)
            if len(photo) > 0:
                raise ValidationError('Please enter another image name , since this name already exists.')


    title = wtf.TextField('Image Title')
    name = wtf.TextField('Image Name')
    x = wtf.HiddenField('x')
    y = wtf.HiddenField('y')
    w = wtf.HiddenField('w')
    h = wtf.HiddenField('h')
    id = wtf.HiddenField('id')


class ManagerPhotos(MethodView):
    """
    Manage photos of gallery.
    """
    def get(self, photo_id):
        """
        Display photo edit page.
        """
        if login.current_user.is_anonymous():
            return redirect('/login/')
        try:
            photo = Photos.objects.get(id=photo_id)
        except ValidationError:
            raise
            return False
        if photo:
            form = ManagerPhotosForm(request.form, photo)

            if request.args.get('target') is not None:
                referrer = request.args.get('target')
            else:
                referrer = url_for('photo_manager')

            timestamp = int(time.time())
            return render_template('manager/admin_gallery_photos_new.html',
                                   form=form,
                                   photo=photo,
                                   referrer=referrer,
                                   timestamp=timestamp)
        else:
            return False


    def post(self, photo_id):
        """
        To crop the image.
        """
        photo = Photos.objects.get(id=photo_id)
        form = ManagerPhotosForm(request.form, photo)
        if form.validate():
            title = request.form.get('title')
            name = request.form.get('name')
            x = int(request.form.get('x', 0) or 0)
            y = int(request.form.get('y', 0) or 0)
            w = int(request.form.get('w', 0) or 0)
            h = int(request.form.get('h', 0) or 0)
            next_page = request.args.get('next')

            photo = Photos.objects.get(id=photo_id)

            photo.title = title.strip()
            photo.name = name.strip()
            photo.save()
            imageFile = photo.file_path
            # have added none check, as 0 is a valid value and should be allowed
            if (x or x == 0) and (y or y == 0) and (w or w == 0) and (h or h == 0):
                img = Image.open(imageFile)
                just_image = img.copy()
                # left, upper, right, lower
                if x < 0:
                    w += -1*x
                    x = 0
                if y < 0:
                    h += -1*y
                    y = 0
                if w > img.size[0]:
                    w = img.size[0]
                if h > img.size[1]:
                    h = img.size[1]
                box = (x, y, x + w, y + h)
                if any(box):
                    try:
                        # remove exitings files other then the original image
                        filelist = [ f for f in os.listdir(os.path.dirname(photo.file_path)) ]
                        for f in filelist:
                            filename = os.path.join(os.path.dirname(photo.file_path), f)
                            if not filename == photo.file_path:
                                os.remove(filename)
                        width, height = Photos.SIZES['cropped']
                        offset_x = max((width - w) / 2, 0)
                        offset_y = max((height - h) / 2, 0)

                        if offset_x > 0 or offset_y > 0:
                            # create the image object to be the final product
                            region = Image.new(
                                # HEX color: #DCD598
                                mode='RGBA', size=(width, height), color=(220, 213, 152, 0))
                            # paste the thumbnail into the full sized image
                            just_image = just_image.crop(box)
                            region.paste(just_image, (offset_x, offset_y))
                        else:
                            region = just_image.crop(box)

                        temp = photo.file_path.rpartition('.')
                        cropped_file_path = photo._cropped_filepath()
                        region.save(cropped_file_path)
                    except SystemError:
                        flash('Error while cropping the image.', 'error')
            flash('Photo edited successfully.', 'success')
            if next_page:
               return redirect(next_page)
            else:
               return redirect(url_for('photo_manager'))
        else:
            flash(form.errors.get('name')[0], 'error')
            return redirect(request.path)


class ChangePhotoOrder(MethodView):
    """
    Class to change photo order in photo gallery.
    """
    def post(self, gallery_id):
        """
        Change photo order in gallery.
        """
        ordered_photo_ids = request.values.getlist('data-id')
        gallery = Gallery.objects.get(id=gallery_id)
        gallery.photos = []
#         gallery.save()
        gallery.photos = self._reordered_photos(ordered_photo_ids)
        gallery.save()
        return 'OK'

    def _reordered_photos(self, ordered_photo_ids):
        """
        Returns list of photos in new order.
        """
        photo_list = []
        photos = Photos.objects.all()
        for id in ordered_photo_ids:
            for photo in photos:
                if str(photo.id) == id:
                    photo_list.append(photo)
        return photo_list


class ManagAllPhotos(MethodView):
    """
    Manage all photos in Photos model.
    """
    def get(self, action=None, photo_id=None):
        """
        Displays all photos from Photos model.
        """
        if login.current_user.is_anonymous():
            return redirect('/login/')
        photos = Photos.objects.all().order_by("-created_time")
        if action == 'delete':
            return self.delete_photo(photo_id)

        timestamp = int(time.time())
        return render_template('manager/admin_photos.html',
                                   photos=photos,
                                   timestamp=timestamp)

    def post(self):
        """
        Stores photo to file system & make entry to the database.
        """
        return_data = {}
        files = []
        pics = request.files.getlist("files[]")
        titles = request.form.getlist('title[]')

        for pic, title in zip(pics, titles):
            if pic.filename:
                photo = Photos(title=title)
                photo.save()
                filepath = os.path.join(str(photo.id), pic.filename)
                photo.path = filepath
                photo.save()
                try:
                    os.makedirs(os.path.dirname(photo.file_path))
                    pic.save(photo.file_path)

                    file_info = {
                        "name": pic.filename,
                        "size": 0,
                        "url": photo.url,
                        "thumbnailUrl": photo.thumbnail,
                        "deleteType": "DELETE"
                      }
                    files.append(file_info)
                except OSError:
                    pic.save(photo.file_path)

        return_data["files"] = files
        return json.dumps(return_data)


    def delete_photo(self, photo_id):
        """
        Delete photo from Phot model & from file system.
        """
        try:
            photo = Photos.objects.get(id=photo_id)
        # TODO: Need to remove photos from exitsing gallery on deletion of photo from the system
            if photo:
                photo.delete()
        except:
            flash('Photo does not exist.', 'error')
            return redirect(request.referrer)

        flash('Photo deleted successfully.', 'success')
        return redirect(request.referrer)


class ManagerIndex(MethodView):
    def get(self):
        if login.current_user.is_anonymous():
            return redirect(url_for('login'))
        activities = Activity.objects().order_by('-created_at').limit(20)
        return render_template('manager/admin_index.html',
                               activities=activities)

class BuildingIndex(MethodView):
    def get(self):
        return redirect('/admin/buildings/')

app.add_url_rule('/admin/residentials/<listing_id>/gallery/', view_func=ManagerListingGallery.as_view('listing_gallery'), methods=['GET', 'POST'])
app.add_url_rule('/admin/residentials/<listing_id>/gallery/<action>/<photo_id>/', view_func=ManagerListingGallery.as_view('delete_listing_photo'), methods=['GET'])

app.add_url_rule('/admin/photos/', view_func=ManagAllPhotos.as_view('photo_manager'), methods=['GET', 'POST'])
app.add_url_rule('/admin/photos/<photo_id>/<action>/', view_func=ManagAllPhotos.as_view('photo_manager_delete'), methods=['GET', 'POST'])

app.add_url_rule('/admin/gallery/<gallery_id>/photo-order/', view_func=ChangePhotoOrder.as_view('change_photo_order'), methods=['POST'])
app.add_url_rule('/admin/photos/<photo_id>/', view_func=ManagerPhotos.as_view('edit_photo'), methods=['GET', 'POST'])

app.add_url_rule('/admin/building/<building_id>/gallery/', view_func=ManagerBuildingGallery.as_view('building_gallery'), methods=['GET'])
app.add_url_rule('/admin/building/<building_id>/gallery/<action>/<photo_id>/', view_func=ManagerBuildingGallery.as_view('delete_building_photo'), methods=['GET'])

app.add_url_rule('/admin/gallery/<gallery_id>/', view_func=ManagerGallery.as_view('manage_gallery'), methods=['GET', 'POST'])
app.add_url_rule('/admin/gallery/<gallery_id>/<action>/<photo_id>/', view_func=ManagerGallery.as_view('manage_gallery_delete'), methods=['GET'])


app.add_url_rule('/admin/residential/unit_info_photos/<unit_id>', view_func=ManagerListUnitPhotos.as_view('residentials_units_photos'), methods=['GET'])
app.add_url_rule('/admin/residential/unit_info/<building_id>', view_func=ManagerListUnits.as_view('residentials_units'), methods=['GET'])


managers.add_url_rule('/', view_func=ManagerIndex.as_view('managerindex_landing'))
managers.add_url_rule('/admin/', view_func=ManagerIndex.as_view('managerindex'))
managers.add_url_rule('/admin/buildings/', view_func=ManagerBuildings.as_view('managerbuildings'))
managers.add_url_rule('/admin/buildings/<action>/', view_func=ManagerBuildings.as_view('managerbuildingsnew'))
managers.add_url_rule('/admin/buildings/<action>/<building_id>/', view_func=ManagerBuildings.as_view('managerbuildingsedit'))
managers.add_url_rule('/admin/buildings/<action>/<building_id>/<photo_index>/', view_func=ManagerBuildings.as_view('managerbuildingseditcrop'))
managers.add_url_rule('/admin/building_features/', view_func=ManagerBuildingFeatures.as_view('managerbuildingfeatures'))
managers.add_url_rule('/admin/building_features/<action>/', view_func=ManagerBuildingFeatures.as_view('managerbuildingfeatures'))
managers.add_url_rule('/admin/building_features/<action>/<building_feature_id>/', view_func=ManagerBuildingFeatures.as_view('managerbuildingfeatures'))

managers.add_url_rule('/admin/unit_features/', view_func=ManagerUnitFeatures.as_view('managerunitfeatures'))
managers.add_url_rule('/admin/unit_features/<action>/', view_func=ManagerUnitFeatures.as_view('managerunitfeatures'))
managers.add_url_rule('/admin/unit_features/<action>/<unit_feature_id>/', view_func=ManagerUnitFeatures.as_view('managerunitfeatures'))

managers.add_url_rule('/admin/views/', view_func=ManagerViews.as_view('managerviews'))
managers.add_url_rule('/admin/views/<action>/', view_func=ManagerViews.as_view('managerviews'))
managers.add_url_rule('/admin/views/<action>/<view_id>/', view_func=ManagerViews.as_view('managerviews'))

managers.add_url_rule('/admin/delete_building_pic/<building_id>/<pic>/', view_func=ManagerBuildingsDeletePic.as_view('managerbuildingsdeletepic'))

managers.add_url_rule('/admin/boroughs/<action>/', view_func=ManagerBorough.as_view('managerboroughnew'))
managers.add_url_rule('/admin/boroughs/<action>/<borough_id>/', view_func=ManagerBorough.as_view('managerboroughedit'))

managers.add_url_rule('/admin/neighborhoods/', view_func=ManagerNeighborhood.as_view('managerneighborhoods'))
managers.add_url_rule('/admin/neighborhoods/<action>/', view_func=ManagerNeighborhood.as_view('managerneighborhoodsnew'))
managers.add_url_rule('/admin/neighborhoods/<action>/<neighborhood_id>/', view_func=ManagerNeighborhood.as_view('managerneighborhoodsedit'))

managers.add_url_rule('/admin/crop/<model_type>/<model_id>/<photo_index>/', view_func=ManagerCropImage.as_view('managercropimage'))
managers.add_url_rule('/admin/crop/<model_type>/<model_id>/<photo_index>/<skip>/', view_func=ManagerCropImage.as_view('managercropimage'))

managers.add_url_rule('/admin/users/', view_func=ManagerUsers.as_view('managerusers'))
managers.add_url_rule('/admin/users/<action>/', view_func=ManagerUsers.as_view('managerusersnew'))
managers.add_url_rule('/admin/users/<action>/<user_id>/', view_func=ManagerUsers.as_view('managerusersedit'))

managers.add_url_rule('/admin/building/photos/<string:building_id>', view_func=BuildingPhotos.as_view("admin_building_photos"))


managers.add_url_rule('/admin/residentials/', view_func=ManagerResidential.as_view('managerresidentials'))
managers.add_url_rule('/admin/residentials/<action>/', view_func=ManagerResidential.as_view('managerresidentialsnew'))
managers.add_url_rule('/admin/residentials/<action>/<residential_id>/', view_func=ManagerResidential.as_view('managerresidentialsedit'))
managers.add_url_rule('/admin/residentials/<action>/<residential_id>/<photo_index>/', view_func=ManagerResidential.as_view('managerresidentialseditcrop'))
managers.add_url_rule('/admin/residentials/available/<available_id>/<available>/', view_func=ManagerResidentialAvailable.as_view('managerresidentialsavailable'))
managers.add_url_rule('/admin/delete_residential_pic/<residential_id>/<pic>/', view_func=ManagerResidentialsDeletePic.as_view('managerresidentialsdeletepic'))
