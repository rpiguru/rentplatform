from itertools import chain
from flask import Blueprint, render_template, make_response, abort
from flask.views import MethodView
from models import (
    Building, ResidentialListing, rentals_characteristics, render_rentals_xml)
from datetime import datetime

home = Blueprint('home', __name__, template_folder="templates")


class OlrFeed(MethodView):
    """
    Feeds for OLR
    """

    def __init__(self, template_name='home/olr_feed/feed.xml'):
        self.template_name = template_name
        super(OlrFeed, self).__init__()

    def get_buildings(self, *args, **kwargs):
        return Building.objects.all()

    @staticmethod
    def update_street_easy_features(existing, objects):
        for feature in objects:
            if feature.street_easy:
                if feature.street_easy_other:
                    existing['other'] = existing.get('other', []) + [feature.street_easy]
                elif not existing.get(feature.street_easy, None):
                    existing[feature.street_easy] = feature
        return existing

    def get_listings(self):
        return ResidentialListing.objects.filter(avail=True)

    def get(self, *args, **kwargs):
        import locale
        locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
        l = []
        buildings = {b.id: b for b in self.get_buildings(*args, **kwargs)}
        for id, building in buildings.items():
            street_easy_features = {}
            rentals_features = {}

            self.update_street_easy_features(
                street_easy_features, building.unit_feature_objects)
            self.update_street_easy_features(
                street_easy_features, building.extra_objects)
            for obj in chain(building.unit_feature_objects, building.extra_objects):
                rentals_characteristics(rentals_features, obj)
            buildings[id] = (building, street_easy_features, rentals_features)

        listings = self.get_listings().filter(building__in=buildings.keys())
        for unit in listings:
            building, street_easy_features, rentals_features = buildings[unit.building]

            self.update_street_easy_features(
                street_easy_features, unit.spec_objects)
            for obj in unit.spec_objects:
                rentals_characteristics(rentals_features, obj)
            unit.other_street_easy_features = u','.join(set(street_easy_features.pop('other', [])))
            unit.street_easy_features = street_easy_features
            unit.rentals_features_xml = render_rentals_xml(rentals_features)
            unit.building = building
            if unit.sqft:
                unit.sqft = locale.format("%d", unit.sqft, grouping=True)
            unit.price = unit.price
            if unit.modified_date is not None:
                unit.modified_date = unit.modified_date.strftime("%Y-%m-%d")
            if unit.availability is not None:
                unit.availability_date = unit.availability
                unit.availability = unit.availability.strftime("%Y-%m-%d")
            l.append(unit)

        today = datetime.now()
        today = today.strftime('%Y-%d-%m')
        feed = render_template(self.template_name, listings=l, today=today)
        response = make_response(feed)
        response.headers["Content-Type"] = "application/xml"
        return response


class BuildingFeed(OlrFeed):
    """
    Singular feed for a building that's specified by name in the URL.
    """
    def get_buildings(self, name, *args, **kwargs):
        buildings = Building.objects.filter(name=name)
        if len(buildings) == 0:
            abort(404)
        return buildings


class RentalsFeed(OlrFeed):
    """
    Feed for buildings that have a rentals.com integration.
    """
    def get_listings(self):
        return ResidentialListing.objects

    def get_buildings(self, *args, **kwargs):
        buildings = super(RentalsFeed, self).get_buildings(*args, **kwargs)
        buildings = buildings.filter(
            rentals_id__nin=[None, ""],
        )
        return buildings


home.add_url_rule('/service/feed/olr/', view_func=OlrFeed.as_view('olr_feed'))
home.add_url_rule('/service/feed/olr/building/<name>/', view_func=BuildingFeed.as_view('olr_building_feed'))

home.add_url_rule('/service/feed/street_easy/', view_func=OlrFeed.as_view(
    'street_easy_feed', template_name='home/street_easy.xml'))
home.add_url_rule('/service/feed/apartments/', view_func=OlrFeed.as_view(
    'apartments_feed', template_name='home/apartments.xml'))
home.add_url_rule('/service/feed/street_easy/building/<name>/', view_func=BuildingFeed.as_view(
    'steet_easy_building_feed', template_name='home/street_easy.xml'))

home.add_url_rule('/service/feed/rentals/', view_func=RentalsFeed.as_view(
    'rentals_feed', template_name='home/rentals.xml'))

home.add_url_rule(
    '/service/feed/silver_creative/',
    view_func=OlrFeed.as_view('silver_creative_feed', template_name='home/silver_creative/feed.xml'))
