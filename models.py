from serve import db, app
from flask.ext.login import UserMixin, current_user
from flask import url_for
from mongoengine import signals
import locale, os, shutil, datetime
from PIL import Image


OLR_BUILDING_TYPE_MAP = {
    "development site": "DV",
    "garage": "GR",
    "hotel": "HO",
    "high-rise": "HR",
    "house": "HS",
    "loft": "LO",
    "low-rise": "LR",
    "mid-rise": "MR",
    "parking lot": "PL",
    "townhouse": "TH",
    "vacant log": "VL",
}

OLR_APARTMENT_SIZE_MAP = {
    "0": "1",
    "1": "7",
    "2": "H",
    "3": "L",
    "4": "M",
    "5": "N",
}

STREET_EASY_BUILDING_TYPES = {
    "condo", "townhouse", "coop", "condop", "rental", "house"
}


def rentals_characteristics(collection, item):
    tags = item.rentals
    parent = item.rentals_parent
    value = item.rentals_value
    tags = tags or u""
    if tags:
        for tag in tags.split(u','):
            set_value(collection, parent, tag, value)
    return collection


def render_single_tag(tag, value):
    return "<{}>{}</{}>".format(tag, value, tag)


def render_tags(key, tags):
    output = ""
    if key == "__all__":
        for tag, value in tags.iteritems():
            output += render_single_tag(tag, value)
    else:
        output += "<{}>".format(key)
        for tag, value in tags.iteritems():
            if isinstance(value, list):
                for v in value:
                    output += render_single_tag(tag, v)
            elif isinstance(value, dict):
                output += render_tags(tag, value)
            else:
                output += render_single_tag(tag, value)
        output += "</{}>".format(key)
    return output


def render_rentals_xml(collection):
    output = ""
    if isinstance(collection, str) or isinstance(collection, unicode):
        return collection
    for key, tags in collection.iteritems():
        output += render_tags(key, tags)
    return output


def set_value(collection, parent, tag, value):
    if not parent:
        parent = "__all__"
    if '/' in parent:
        levels = parent.split('/')
        container = collection

        for index, level in enumerate(levels):
            if level not in container:
                container[level] = {}
            container = container[level]
        container[tag] = value
    else:
        if parent not in collection:
            collection[parent] = {}
        collection[parent][tag] = value
    return collection


def handler(event):
    """Signal decorator to allow use of callback functions as class decorators."""

    def decorator(fn):
        def apply(cls):
            event.connect(fn, sender=cls)
            return cls

        fn.apply = apply
        return fn

    return decorator


@handler(signals.post_save)
def create_activity_on_save(sender, document, created):
    action = "Created" if created else "Edited"
    try:
        user = current_user.login
    except RuntimeError:
        user = "System"
    except AttributeError:
        user = "Feed"
    Activity(
        created_at=datetime.datetime.now(),
        user=user,
        content=u'{} {}: {}'.format(action, sender.__name__, document)
    ).save()


@handler(signals.post_delete)
def create_activity_on_delete(sender, document):
    try:
        user = current_user.login
    except RuntimeError:
        user = "System"
    Activity(
        created_at=datetime.datetime.now(),
        user=user,
        content=u'Deleted {}: {}'.format(sender.__name__, document)
    ).save()


@create_activity_on_save.apply
@create_activity_on_delete.apply
class User(UserMixin, db.Document):
    login = db.StringField(max_length=80, unique=True)
    email = db.StringField(max_length=120, required=True)
    password = db.StringField(max_length=64, required=True)
    company = db.StringField(max_length=120, required=True)
    tele = db.StringField()
    name = db.StringField(required=True)
    is_super_user = db.BooleanField(default=False)
    is_admin = db.BooleanField(default=False)

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __unicode__(self):
        return self.login


class Activity(db.Document):
    created_at = db.DateTimeField(required=True)
    content = db.StringField(required=True)
    user = db.StringField(required=True)

@create_activity_on_save.apply
@create_activity_on_delete.apply
class Photos(db.Document):
    """
    Keep records of photos.
    """
    title = db.StringField(max_length=100)
    name = db.StringField(max_length=100)
    path = db.StringField()
    active = db.BooleanField(default=True)
    created_time = db.DateTimeField(default=datetime.datetime.now)

    SIZES = {
        'thumb': (100, 75),
        'small': (275, 150),
        'med': (400, 300),
        'big': (800, 600),
        'cropped': (800, 600)
    }
    
    crop_prefix = "cropped_"
    
    def _cropped_filepath(self):
        """
        returns the filename of cropped version
        """
        filename = "{}{}".format(self.crop_prefix, os.path.basename(self.file_path))
        return os.path.join(os.path.dirname(self.file_path), filename)

    @property
    def _filename(self):
        """
        returns the filename
        """
        return os.path.basename(self.file_path)

    @property
    def file_path(self):
        """
        returns the absolute path for the file
        """
        if self.path:
            return os.path.join(app.config['UPLOADED_PHOTOSIMAGES_DEST'], self.path)
        else:
            return ""

    def delete(self):
        """
        Overrided this function to delete the photo directory & its contents through file system.
        """
        dir_path = os.path.dirname(self.file_path)
        file_path = self.file_path
        # Deletes Img From File System If Present
        if os.path.isfile(file_path):
            try:
                shutil.rmtree(dir_path)
            except OSError :
                print "Error: can\'t find file or read data"

#         To remove referance of photo from Gallery
        galleries = Gallery.objects.all()
        for gallery in galleries:
            for photo in gallery.photos:
                if str(self.id) == str(photo.id):
                    gallery.photos.remove(photo)
            gallery.save()

        return super(Photos, self).delete()

    @property
    def url(self):
        """
        returns url for photo
        """
        return url_for("static", filename='uploads/photos/%s' % self.path)

    @property
    def thumbnail(self):
        thumbnail_filepath = os.path.join(os.path.dirname(self.file_path),
                                              os.path.basename("thumb_" + self._filename))
        if not os.path.isfile(thumbnail_filepath):
            width, height = 100, 75
            img = Image.open(self.file_path)
            resized_img = img.resize((width, height), Image.BILINEAR)
            resized_img.save(thumbnail_filepath)

        _url = os.path.join(os.path.dirname(self.url), "thumb_" + self._filename)
        return _url

    def get_photo_url(self, photo_size):
        """
        Returns url for different sized photos.
        """
        photo_filepath = os.path.join(os.path.dirname(self.file_path),
                                              os.path.basename(photo_size + "_" + self._filename))

        if not os.path.isfile(photo_filepath):
            width, height = self.SIZES[photo_size][0], self.SIZES[photo_size][1]
            cropped_file_path = self._cropped_filepath()
            try:
                img = Image.open(cropped_file_path)
            except IOError:
                try:
                    img = Image.open(self.file_path)
                except IOError:
                    img = None
            if img:
                offset_x = max((width - img.size[0]) / 2, 0)
                offset_y = max((height - img.size[1]) / 2, 0)

                if offset_x > 0 or offset_y > 0:
                    # create the image object to be the final product
                    resized_img = Image.new(
                        # HEX color: #DCD598
                        mode='RGBA', size=(width, height), color=(220, 213, 152, 0))
                    # paste the thumbnail into the full sized image
                    resized_img.paste(img, (offset_x, offset_y))
                else:
                    resized_img = img.resize((width, height), Image.BILINEAR)
                resized_img.save(photo_filepath)

        _url = os.path.join(os.path.dirname(self.url), photo_size + "_" + self._filename)
        return _url

    @property
    def get_thumb(self):
        """
        Returns url for thumbnail.
        """
        thumb_url = self.get_photo_url('thumb')
        return thumb_url

    @property
    def get_small(self):
        """
        Returns url for small image.
        """
        small_img_url = self.get_photo_url('small')
        return small_img_url

    @property
    def get_med(self):
        """
        Returns url for medium image.
        """
        med_img_url = self.get_photo_url('med')
        return med_img_url

    @property
    def get_big(self):
        """
        Returns url for big image.
        """
        big_img_url = self.get_photo_url('big')
        return big_img_url

    @property
    def get_cropped(self):
        """
        Returns cropped version of image.
        """
        cropped_url = self.get_photo_url('cropped')
        return cropped_url

    def get_name(self):
        """
        Returns name of image.
        """
        if self.name:
            return self.name
        else:
            return self._filename

    def get_cropped_or_original(self):
        """
        Returns cropped version or original version of image.
        """
        cropped_url = self.get_photo_url('cropped')
        if cropped_url == "":
            url = os.path.join(os.path.dirname(self.url), self._filename)
            return url
        return cropped_url

    def __unicode__(self):
        return self.title or self.file_path.split('/')[-1]


@create_activity_on_save.apply
@create_activity_on_delete.apply
class Gallery(db.Document):
    """
    Keep records of photo gallery.
    """
    title = db.StringField(max_length=100)
    photos = db.ListField(db.ReferenceField(Photos, dbref=True))

    def __unicode__(self):
        return self.title or ""


@create_activity_on_save.apply
@create_activity_on_delete.apply
class Borough(db.Document):
    name = db.StringField(required=True)

    def __unicode__(self):
        return self.name


@create_activity_on_save.apply
@create_activity_on_delete.apply
class Neighborhood(db.Document):
    name = db.StringField(required=True)
    desc = db.StringField()
    borough = db.ReferenceField(Borough, dbref=True, required=True)

    def __unicode__(self):
        return self.name

@create_activity_on_save.apply
@create_activity_on_delete.apply
class BuildingType(db.Document):
    name = db.StringField(required=True)

    def __unicode__(self):
        return self.name

    @property
    def olr(self):
        try:
            return OLR_BUILDING_TYPE_MAP["high-rise"]
        except KeyError:
            return ""

    @property
    def street_easy(self):
        return "rental"


@create_activity_on_save.apply
@create_activity_on_delete.apply
class Extra(db.Document):
    name = db.StringField(required=True)
    street_easy = db.StringField()
    street_easy_other = db.BooleanField(default=False)
    rentals = db.StringField()
    rentals_parent = db.StringField()
    rentals_value = db.StringField()

    def __unicode__(self):
        return self.name

    @property
    def street_easy_tag(self):
        if not self.street_easy:
            return ""
        return u"<{}></{}>".format(self.street_easy, self.street_easy)


@create_activity_on_save.apply
@create_activity_on_delete.apply
class UnitFeature(db.Document):
    name = db.StringField(required=True)
    street_easy = db.StringField()
    street_easy_other = db.BooleanField(default=False)
    rentals = db.StringField()
    rentals_parent = db.StringField()
    rentals_value = db.StringField()

    def __unicode__(self):
        return self.name

    @property
    def street_easy_tag(self):
        if not self.street_easy:
            return ""
        return u"<{}></{}>".format(self.street_easy, self.street_easy)


@create_activity_on_save.apply
@create_activity_on_delete.apply
class View(db.Document):
    name = db.StringField(required=True)

    def __unicode__(self):
        return self.name


@create_activity_on_save.apply
@create_activity_on_delete.apply
class Building(db.Document):
    building_type = db.ReferenceField(BuildingType, dbref=True, required=True)
    name = db.StringField(required=True)
    entity = db.StringField(required=True)
    address = db.StringField(required=True)
    lat = db.StringField()
    lng = db.StringField()
    city = db.StringField(required=True)
    zip_code = db.StringField(max_length=5, required=True)
    borough = db.ReferenceField(Borough, dbref=True, required=True)
    neighborhood = db.ReferenceField(Neighborhood, dbref=True, required=True)
    state = db.StringField(max_length=2, required=True)
    agent_phone = db.StringField(required=True)
    agent_email = db.StringField(required=True)
    agent_name = db.StringField(required=True)
    year = db.StringField(max_length=4)
    floors = db.IntField()
    units = db.IntField()
    description = db.StringField(required=True)
    extras = db.ListField(db.ObjectIdField())
    unit_features = db.ListField(db.ObjectIdField())
    link = db.StringField(max_length=250)
    gallery = db.ReferenceField(Gallery, dbref=True)
    rentals_id = db.StringField()

    def __unicode__(self):
        return self.name

    @property
    def extra_objects(self):
        return Extra.objects.filter(id__in=self.extras)

    @property
    def unit_feature_objects(self):
        return UnitFeature.objects.filter(id__in=self.unit_features)

    def get_big(self, photo):
        return 'uploads/buildingphotos/big/%s/' % photo

    def get_small(self, photo):
        return 'uploads/buildingphotos/small/%s/' % photo

    def get_med(self, photo):
        return 'uploads/buildingphotos/med/%s/' % photo

    def get_url_for(self, photo):
        return 'uploads/buildingphotos/%s/' % photo

    def get_imgs_src(self):
        return [{'pic' : pic, 'url' : 'uploads/buildingphotos/%s' % pic} for pic in self.gallery.photos]

    def photo_urls(self, photo_size='med'):
        """
        Returns the urls of photos for Unit. If there are no photos of unit return the list 
        of photos of the building.
        """
        urls = []

        if photo_size == 'org':
            if self.photos:
                for photo in self.photos:
                    urls.append(url_for('static', filename='uploads/buildingphotos/%s/' % photo))
        else:
            if self.photos:
                for photo in self.photos:
                    urls.append(url_for('static', filename='uploads/buildingphotos/%s/%s/' % (photo_size, photo)))

        return urls

    def get_gallery_photo_urls(self):
        """ 
        Returns list of urls for gallery photos.
        """
        urls = []
        for photo in self.gallery.photos:
            photo_path = {
                          'photo_id' : photo.id,
                          'url' : url_for('static', filename='uploads/gallery/%s/%s/%s/%s' % (self.id, self.gallery.id, photo.id, photo.path))
                         }
            urls.append(photo_path)
        return urls

    def get_gal_photo_url(self, path, size):
        """
        Returns url for photo of different sizes.
        """
        url = ""
        for photo in self.gallery.photos:
            if str(path) == photo.url:
                if size == "thumb":
                    url = photo.get_thumb
                if size == "small":
                    url = photo.get_small
                if size == "med":
                    url = photo.get_med
                if size == "big":
                    url = photo.get_big
        return url


@create_activity_on_save.apply
@create_activity_on_delete.apply
class ResidentialListing(db.Document):
    building = db.ObjectIdField()
    floor = db.IntField(min_value=0, max_value=1000)
    floorplan = db.StringField()
    floorplan_pdf = db.StringField()
    apt_num = db.StringField()
    price = db.IntField()
    description = db.StringField()
    bed = db.IntField(min_value=0, max_value=500, required=True)
    bath = db.FloatField(min_value=0, max_value=500, required=True)
    sqft = db.IntField(min_value=0, required=False)
    avail = db.BooleanField()
    specs = db.ListField(db.ObjectIdField())
    views = db.ListField(db.ObjectIdField())
    notes = db.StringField()
    availability = db.DateTimeField()
    photo_gallery = db.ReferenceField(Gallery, dbref=True)
    modified_date = db.DateTimeField()
    url = db.StringField(max_length=500)
    owner_pays = db.BooleanField(default=False)

    def __unicode__(self):
        return self.apt_num

    def get_big(self, photo):
        return 'uploads/residentialphotos/big/%s' % photo

    def get_small(self, photo):
        return 'uploads/residentialphotos/small/%s' % photo

    def get_med(self, photo):
        return 'uploads/residentialphotos/med/%s' % photo

    def get_url_for(self, photo):
        return 'uploads/residentialphotos/%s' % photo

    def get_url_for_floorplan(self, photo):
        return 'uploads/floorplans/%s' % photo

    def get_imgs_src(self, size=""):
        if self.photo_gallery:
            if size:
                return [{'pic' : pic, 'url' : 'uploads/residentialphotos/%s/%s' % (size, pic)} for pic in self.photo_gallery.photos]
            else:
                return [{'pic' : pic, 'url' : 'uploads/residentialphotos/%s' % pic} for pic in self.photo_gallery.photos]

    def get_building(self):
        if self.building :
            return Building.objects(id=self.building).first()
        return None

    @property
    def spec_objects(self):
        return UnitFeature.objects.filter(id__in=self.specs)

    @property
    def view_objects(self):
        return View.objects.filter(id__in=self.views)

    def save(self, *args, **kwargs):
        if not self.photo_gallery:
            gallery = Gallery()
            gallery.save()
            self.photo_gallery = gallery 
        
        super(ResidentialListing, self).save(*args, **kwargs)
    
    @property
    def building_object(self):
        """
        Returns building object
        """
        if hasattr(self, "_building") and self._building:
            pass
        else:
            if isinstance(self.building, Building):
                self._building = self.building
            else:
                self._building = Building.objects.get(id=self.building)
        return self._building

    @property
    def gallery(self):
        """
        Returns the list of photos for Unit. If there are no photos of unit return the list 
        of photos of the building
        """
        return self.photo_gallery.photos

    def photo_urls(self, photo_size='med'):
        """
        Returns the urls of photos for Unit. If there are no photos of unit return the list 
        of photos of the building.
        """
        urls = []

        if photo_size == 'org':
            if self.photos:
                for photo in self.photos:
                    urls.append(url_for('static', filename='uploads/residentialphotos/%s/' % photo))
            else:
                for photo in self.building_object.photos:
                    urls.append(url_for('static', filename='uploads/buildingphotos/%s/' % photo))
        else:
            if self.photos:
                for photo in self.photos:
                    urls.append(url_for('static', filename='uploads/residentialphotos/%s/%s/' % (photo_size, photo)))
            else:
                for photo in self.building_object.photos:
                    urls.append(url_for('static', filename='uploads/buildingphotos/%s/%s/' % (photo_size, photo)))

        return urls

    def photo_url_set(self):
        """
        Returns the set of 4 types of url for image (i.e. url for big, small, medium & original image).
        """
        big_img_urls = self.photo_urls('big')
        small_img_urls = self.photo_urls('small')
        med_img_urls = self.photo_urls('med')
        org_urls = self.photo_urls('org')

        def pic_set(*args):
            a = {
                 "big": args[0],
                 "med": args[1],
                 "small": args[2],
                 "org": args[3],
            }
            return a

        url_set = map(pic_set, big_img_urls, med_img_urls, small_img_urls, org_urls, [""] * len(med_img_urls))

        return url_set

    @property
    def price_locale_format(self):
        """
        Returns price in locale format.
        """
        locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
        return locale.format("%d", self.price, grouping=True)

    @property
    def bath_str(self):
        """
        returns the float value in str format
        """
        num = ""
        if self.bath:
            num = ("%.1f" % self.bath).rstrip("0").rstrip(".")
        return num

    @property
    def print_bath(self):
        """
        Returns the printable string for bathroom
        """
        printable_str = ""
        if self.bath:
            num = ("%.1f" % self.bath).rstrip("0").rstrip(".")
            printable_str = "{} BATH".format(num)
        return printable_str

    @property
    def get_avail_date(self):
        """
        Returns current date if availability date is less than or equal to current date.
        """
        import datetime
        current_date = datetime.datetime.today()
        if self.availability:
            if current_date > self.availability:
                avail_date = current_date
            else:
                avail_date = self.availability
        else:
            avail_date = current_date
        return avail_date

    @property
    def get_absolute_url(self):
        """
        returns the url for residential listing
        """
        component1 = "_".join(self.apt_num.split(' ')).lower()
        component2 = "_".join(self.building_object.address.split(' ')).lower()
        url1 = "{}@{}".format(component1, component2)
        url = url_for('unit_detail_info', obj_id=url1)
        return url
    
    def get_numrooms(self):
        """
        returns total number of rooms.. 
        calculated as :NUMBER OF ROOMS = Number of Bedrooms (existing field in CMS) + 2
        """
        return self.bed + 2

    @property
    def olr_apartment_size(self):
        try:
            return OLR_APARTMENT_SIZE_MAP[str(self.bed)]
        except KeyError:
            return ""
