

Appliction Requirements
========

Application is built on Flask. All the python packages are mentioned in requirements.txt

Database used is mongodb version 2.2
follow the instructions at below url for installation of version 2.2
http://docs.mongodb.org/v2.2/tutorial/install-mongodb-on-ubuntu/

application is running on gunicorn with proxy through ngnix

how to reload the application:
stop the child process for gunicorn:
kill -HUP `ps -ef | grep gunicorn.*8001 | head -n 1 | tr -s ' ' | cut -f2 -d' '`

start the parnet process if killed:
cd rentplatform; python /usr/local/bin/gunicorn serve:app -b 127.0.0.2:8001 --daemon

-