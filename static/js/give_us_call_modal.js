//block to handle lightbox for GIVE US CALL feature
  $(function giveUsCall(){
        //GIVE US CALL
        $(".give-us-call").on('click', function(e){
          var style_elem = $("<style id='myid'>").text(".modal-backdrop{background-color:#FFFFFF;}.modal-backdrop, .modal-backdrop.fade.in{opacity:0.6;}");
          $("head").append(style_elem);
          
          $("#give-us-call-modal").modal();
            var url = building_info.building.search_contact_url;
            $('#give-us-all-modal-body').load(url);
          return false;
        });
        
        //CLOSE GIVE US CALL
        $(".close-give-us-call").click(function(e){
          $("#give-us-call-modal").modal('hide');
          
          return false;
        });
        
        // Function to remove style tag after modal get hidden
        $("#give-us-call-modal").on('hidden', function(){
          $("head").find("style#myid").remove();
        });
  });