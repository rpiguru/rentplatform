"use strict";
var diamond = "/static/img/diamond.png";
var circle = "/static/img/circle.png";
var icon = diamond;

var uniqueId = null;
var getUniqueName = function(prefix){if (!uniqueId) uniqueId = (new Date()).getTime();return (prefix || 'id') + (uniqueId++);};

function hightlight_elem(selector){
	$(".place-names li.active").removeClass("active");
	$("#" + selector).addClass("active");
}


function setup_menu(){
	$(neighborhoods).each(function(index, elem){
		var li = $("<li class='neighborhood-item'  data-rel_menu='menu_link_" + index + "' data-id='" + elem.id +"'>" +  elem.name +  "</li>");	
		if(index===0){
			li.addClass("active");
		}
		$("ul#neighbor-menu").append(li);
	});
}

$(function(){//set the height and width for image area
	$("#map").height($("#neighborhood-description").height());
	setup_menu();	
	inner_list();
	$(".neighborhood-item").on( "click", function(e) {
	var $neighborhood = $(this);
	if(!$neighborhood.hasClass("active")){
		//set the active menu	
		set_active_menu($neighborhood);
	}	
	});
	populateNeighborhood(neighborhoods[0]);	
	set_active_menu($('.neighborhood-item').get(0));
});

function set_active_menu(item){
	var $neighborhood = $(item);
	$(".neighborhood-item").removeClass("active");
		$neighborhood.addClass("active");
		// $(this).addClass("active");
		//zoom the map to that location
		map.setZoom(16,{animate:false});
		var dataID = $neighborhood.attr("data-id");
		showExtraPointsForHotspot(get_neighborhood(dataID));
		map.eachLayer(function(e){
			if(e.hasOwnProperty('feature')){
				if(e.feature.properties.id == dataID){
					e.openPopup();
					map.panTo(e.getLatLng());
				//	map.panBy([375, 0]);
				}
			}
		});
		
		populateNeighborhood(get_neighborhood(dataID));
}

/*
	when user clicks on any place name in right panel, that place will be highlighted in the map
*/
function clickPlace(){
	console.log("clickplace event call");
	$(".place-names li").on("click", function(event){
		if(!$(this).hasClass("active")){
			var elem_id = $(this).attr("id"); 
			hightlight_elem(elem_id);
			
			map.markerLayer.eachLayer(function(marker) {
	        if (marker.feature.properties.id === elem_id) {
	            marker.openPopup();
							map.panTo(marker.getLatLng());
				//			map.panBy([375, 0]);
							return false;
	        }
	    });			
		}
	 return false; 
	});

}

var populateNeighborhood = function(n) {
	$("#neighborhood-name").text(n.name);
	$("#neighborhood-zip").text(n.zip_code);
	$("#neighborhood-lat").text(n.lat);
	$("#neighborhood-lng").text(n.lng);
	$("#neighborhood-desc").html('');
	$("#neighborhood-desc").html(n.desc);
	$(".neighborhood_images_box").empty();
	var s = '';
	
	function _setup_gallery(neighborhood){
		var gallery = $("<div>");
		$(neighborhood.photos).each(function(index, elem){
		  try{
          var img_url_big = elem.big;
          var img_url_med = elem.med;
      }catch(Exception){
		    var img_url_big = "";
		    var img_url_med = "";
		  }
			var image_a = $("<a>").addClass("image_gallery").attr({rel:"image_gallery123", href:img_url_med});
			var img = $("<img>").addClass("neighborhood_img123").attr({src:img_url_big, width:"320", height:"236"});
			image_a.append(img);
			if(index >=3){ //display only 3 images
				image_a.css({"display":"none"});
			}
			gallery.append(image_a);
			
		});
		$(".neighborhood_images_box").append(gallery);
		gallery.children().fancybox({
			prevEffect	: 'none',
			nextEffect	: 'none'
		});
		
	}
	_setup_gallery(n);

	$("#neighborhood-extra-points").empty();
	var e = '';
	var categories = {};
	for(e in extraPoints) {
		if (extraPoints[e].neighborhood == n["id"]) {
			var place_info = {"name": extraPoints[e].name, "id": extraPoints[e].uid, 
							"lat": extraPoints[e].lat, "lng": extraPoints[e].lng};
			
			if(!categories.hasOwnProperty(extraPoints[e].category)) {
				categories[extraPoints[e].category.toString()] = [];
				categories[extraPoints[e].category.toString()].push(place_info);
			}else {
			categories[extraPoints[e].category.toString()].push(place_info);
			}
		}
	}
	var c = '';
	for(c in categories) {
		var places = categories[c];
		var $currentUl = $("<li><ul class='place-names'></ul></li>");
		$("#neighborhood-extra-points").append("<li class='place-category'><b>"+ c +"</b></li>");
		$("#neighborhood-extra-points").append($currentUl);
		var place = '';
		for(place in places) {
			// var place_id = "id-" + c.replace(/ /g, "-") + "-" + places[place].replace(/ /g, "-");
			// place_id = place_id.replace("&", "");
			var place_id = places[place].id;
			var place_name = places[place].name;
			var lielem = $("<li>").attr("id", place_id).append($("<a>").attr({"href":""}).text(place_name).data({"lat":places[place].lat, "lng":places[place].lng}));
			// $currentUl.find("ul").append("<li id='" + place_id +"'><a href=''>" + place_name + "</a></li>");
			$currentUl.find("ul").append(lielem);
		}
	}

	clickPlace();
};

	function inner_list(){
		var features = [];
	//adds extra points to features
	$(extraPoints).each(function(index, point){
		icon = diamond;
		if(point.club){
			 icon = circle;
		}
		point.uid = getUniqueName("place-point-");
		features.push({
			type: 'Feature',
			geometry: {
			type: 'Point',
			coordinates: [point.lng, point.lat]
			},
			properties: {
			"icon": {
				"iconUrl" : icon,
				"iconSize": [14, 13],
				"iconAnchor": [18, 47],
				"popupAnchor": [-40, 40]
			},
			"image":icon,
			"extraPoint":true,
			"name": point.name,
			"id": point.uid,
			'neighborhood': point.neighborhood
			}
		});
	});


	//add neighbourhood to features list	
	$(neighborhoods).each(function(index, neighbor){
		features.push({
			type: 'Feature',
			geometry: {
				type: 'Point',
				coordinates: [neighbor.lng, neighbor.lat]
				},
			properties: {
				"icon" : {
					"iconUrl":"/static/img/mapicon.png",
					"iconSize": [20, 31],
					"iconAnchor": [18, 47],
					"popupAnchor": [-95, -25]
				},
				"image":"/static/img/mapicon.png",
				"id":neighbor.id,
				"zip":neighbor.zip_code,
				"name":neighbor.name,
				"desc":neighbor.desc,
				"photos":neighbor.photos.slice(0,3),
				"extraPoint":false,
				"rel_menu": "menu_link_" + index
			}
		});
	});

	var groupedFeatures = (function(){
		var groupedItems = {};
		$(extraPoints).each(function(index, elem){
			if(!groupedItems.hasOwnProperty(elem.neighborhood)){
				groupedItems[elem.neighborhood] = [];
			}
			groupedItems[elem.neighborhood].push(elem);
		});
		return groupedItems;
	})();

	var geoJson = {
		type: 'FeatureCollection',
		"features": features
	};
	
	// Add features to the map
map.markerLayer.setGeoJSON(geoJson);
}

function showExtraPointsForHotspot(neighbourhood){
		map.markerLayer.setFilter(function(f) {
			return true;
		});
}

//returns neighborhood object based on the mongo unique id
function get_neighborhood(data_id){
	var neighborhood = null;
	$(neighborhoods).each(function(index, neighbor){
		if ( neighbor.id == data_id ) {
			neighborhood = neighbor;
			return false;
		}
	});
	return neighborhood;
}

var map = L.mapbox.map('map', 'rentplatform.map-kwvio7ve').setView([ 40.71577741296778, -73.98244857788086], 12);

// Set a custom icon on each marker based on feature properties
map.markerLayer.on('layeradd', function(e) {
    var marker = e.layer,
    feature = marker.feature;

    marker.setIcon(L.icon(feature.properties.icon));
    
    if (marker.feature.properties.extraPoint) {
			var popupContent = '<div class="extrapoint-map-popup bubble">' + feature.properties.name + '</div>';
		}else{
			var popupContent = '<div class="neighborhood-map-popup">' + feature.properties.name + '</div>';	
		}
    marker.bindPopup(popupContent,{
			closeButton: false,
			minWidth: 150,
			maxWidth:150,
			background: '#000000'
		});
    
   
});
//Remove Attribution control From Map
map.removeControl(map.attributionControl);


//show main markers on page load
function initial_markers(){
	map.markerLayer.setFilter(function(feature) {
   	return !feature.properties.extraPoint;
  });
 
 //to open the popup for buildings on page load.
 //this is not workin as there can be only one popup at a time
  /*
  map.markerLayer.eachLayer(function(marker) {
      if(!marker.feature.properties.extraPoint){
          marker.openPopup();
      }
    });*/
  
}
initial_markers();

//to open the popup for extrapoint
map.markerLayer.on('mouseover',function(e){
		e.layer.openPopup();
		return;
});
//top close the popup for extrapoint
map.markerLayer.on('mouseout', function(e) {
			e.layer.closePopup();
			return;
	});


/*repliacte the effect of menu click on clicking the marker*/
map.markerLayer.on('click',function(e){
	if(!e.layer.feature.properties.extraPoint){
		var menu_item = $(".neighborhood-item").filter("[data-rel_menu=" + e.layer.feature.properties.rel_menu + "]");
		// menu_item.trigger("click");
		var hotspot = get_neighborhood(menu_item.data("id"));
		$(".neighborhood-item").removeClass("active");
		menu_item.addClass("active");
		showExtraPointsForHotspot(hotspot);
		map.setZoom(16,{animate:false});
		map.panTo(e.layer.getLatLng());
		//map.panBy([375, 0]);
		
		populateNeighborhood(hotspot);	
	}
});


//sub menu




