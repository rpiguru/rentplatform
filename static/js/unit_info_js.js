
// <!-- Sorting the search results -->

//function to get url variables for sorting the result
    function getUrlVars() {
    if (!window.location.search) {
        return({});   // return empty object
    }
    var parms = {};
    var temp;
    var items = window.location.search.slice(1).split("&");   // remove leading ? and split
    for (var i = 0; i < items.length; i++) {
        temp = items[i].split("=");
        if (temp[0]) {
            if (temp.length < 2) {
                temp.push("");
            }
            parms[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);        
        }
    }
    return(parms);
  }

// assigning search parameters to serachParams 
  var serachParams = {
    "get": function(){
      if(! window._search_params){
        window._search_params = getUrlVars();
      }
      return window._search_params;
    },
    "set":function(params){
      window._search_params = params;
    }
  };
  
// function to sort the search result
  $(function sort_result(){
    $('.sort-field').on("click", function(event){
      var sort = $(this).data("sort");
      var search_url = unit_info.building.search_url;
      var queryParams = serachParams.get();
      queryParams["sort"] = sort;   
      search_url = search_url + "?" + $.param(queryParams);
      serachParams.set(queryParams);
      
      window.location.href = search_url; 
      
    });
  });
  
// function to lode more results
  $(function lode_more(){
    $('#lode-more-button').on("click", function(event){
        var lode_more = $(this).data("loadmore");
        var search_url = unit_info.building.search_url;
        var queryParams = serachParams.get();
        queryParams["loadmore"] = lode_more; 
        search_url = search_url + "?" + $.param(queryParams);
        serachParams.set(queryParams);
        
        window.location.href = search_url; 
        
      });
  });


// <!-- To dislay unit information -->
  $(function() {
    $( "#accordion" ).accordion({ heightStyle: "content" });
  }); 

// <!-- search result slide down on click of refine search on unit details page -->
   $(document).ready(function(){
     $("#refine-result").click(function(){
       $(".search-area").slideToggle(1000);
     });
     
   });


// <!-- Contact agent and send to friend functions used on unit details page -->

  function randomNumber(min, max){
    min = min || 1;
    max = max || 10;
  return (Math.floor(Math.random() * (max - min + 1)) + min);
  }

  function setCaptcha(selector){
    var captchaDiv = $(selector);
    captchaDiv.find("#cap_num_1").text(randomNumber());
    captchaDiv.find("#cap_num_2").text(randomNumber());
  }
    
  function captcaValidation(selector){
    var captchaDiv = $(selector);
    captchaDiv.find("#captcha_error").hide(300);
    captchaDiv.removeClass("error");
    var userInput = captchaDiv.find("input#captcha").val();
    var int_1 = parseInt(captchaDiv.find("#cap_num_1").text());
    var int_2 = parseInt(captchaDiv.find("#cap_num_2").text());
    var sum = int_1 + int_2;
    if(userInput == sum){
      captchaDiv.find("input#captcha").val("");
      return true;
    }else{
      captchaDiv.addClass("error");
      captchaDiv.find("#captcha_error").show(300);
      return false;
    }
  }

//block to handle lightbox for contact Agent feature
  $(function contactAgent(){
    //CONTACT AGENT
    $(".contact-agent").on('click', function(e){
      e.preventDefault();
       var apt = $(this).attr('data-apt');
       var building = $(this).attr('data-building');
       var id = $(this).attr('data-unit-id');
       $("form[name=contact-agent-form]").find("input[name=listing_id]").val(id);
       $("form[name=contact-agent-form]").find("input[name=apt_num]").val(apt);
       $("#contact-agent-modal-message").val("I am interested in apt " + apt + " at " + building + ".");
       setCaptcha("#agent-captcha_control");
      $("#contact-agent-modal").modal();
    });

    //ACTION TO SEND A MAIL WHEN SEND BUTTON IS CLICKED
    $("#contact-agent-action").on('click', function(e){
      var button = $(this);
      var data = {};
      
      data["firstname"] = $("form[name=contact-agent-form]").find("input[name=firstname]").val();
      data["lastname"] = $("form[name=contact-agent-form]").find("input[name=lastname]").val();
      data["email"] = $("form[name=contact-agent-form]").find("input[name=email]").val();
      data["telephone"] = $("form[name=contact-agent-form]").find("input[name=telephone]").val();
      data["message"] = $("form[name=contact-agent-form]").find("textarea[name=message]").val();
      data["listing_id"] = $("form[name=contact-agent-form]").find("input[name=listing_id]").val();
      data["page_type"] = $("form[name=contact-agent-form]").find("input[name=page_type]").val();
      
      
      /*Contact Agent Form Validation*/ 
      var form = $( "#contact-agent-form" );   
      form.validate();
      if(!form.valid()){
        return false;
      }
      var is_captcha_valid = captcaValidation("#agent-captcha_control");
      if( ! is_captcha_valid){
        return false;
      }
      
      var url = unit_info.building.email_agent_url;
      button.attr("disabled", "disabled");
      $("#contact-agent-notification").text("Sending...");
      $.post(url, data, function(response, status, xhr){
        if(data["page_type"]=="building"){
          var eventLabel = "Contacted for Building";
          var eventCategory = "Contact Agent for Building";
        }else{
          var eventLabel = "Contacted for Listing";
          var eventCategory = "Contact Agent for Listing ";
        }
        
   
      	building_name = $("form#contact-agent-form").data("building");
      	apt_num = $("form#contact-agent-form").data("apt");
        ga('send', { 	
            'hitType': 'event',
            'eventCategory':eventCategory,   // Required.
            'eventAction': 'click',      // Required.
            'eventLabel': apt_num + " - " + building_name,
            'page': location.pathname
          }); 
          button.attr("disabled", false);
          
          $("#contact-agent-notification").text("Thank You, Your Message Has Been Sent.");
          setTimeout(function(){
            $("#contact-agent-notification").text("");
          }, 300);

         $("#contact-agent-modal").modal('hide');
      });
    });
    
    //CLOSE CONTACT AGENT
    $(".close-contact-agent").click(function(e){
      e.preventDefault();
      $("#contact-agent-modal").modal('hide');
    });

  });



//function to send to friend modal
  $(function sendToFriend(){
    
    //SEND TO FRIEND
    $(".send-to-friend").on('click',function(e){
      e.preventDefault();
      var apt = $(this).attr('data-apt');
      var building = $(this).attr('data-building');
      var id = $(this).attr("data-unit-id");
      var property_url = $(this).attr("data-unit-url");
      $("form#send-to-friend-form").find("input[name=listing_id]").val(id);
      $("#send-to-friend-modal-message").val("Check out apt " + apt + " at " + building + "." + " " + "http://"+ window.location.host + property_url );
      $("#send-to-friend-listing").text(apt + " at " + building + "");
      setCaptcha("#frnd-captcha_control");
      $("#send-to-friend-modal").modal();
    });

    //CLOSE SEND TO FRIEND
    $(".close-send-friend").click(function(e){
      e.preventDefault();
      $("#send-to-friend-modal").modal('hide');
    });

    //ACTION TO SEND A MAIL WHEN SEND BUTTON IS CLICKED
    $("#send-friend-action").on('click', function(e){
      e.preventDefault();
      var button = $(this);
      var email = $("form#send-to-friend-form").find("input[name=email]").val();
      var username = $("form#send-to-friend-form").find("input[name=fromname]").val();
      var message = $("form#send-to-friend-form").find("textarea[name=message]").val();
      var id = $("form#send-to-friend-form").find("input[name=listing_id]").val();
      var page_type = $("form#send-to-friend-form").find("input[name=page_type]").val();
      var fdata = {"email":email,
            "username": username,
            "message": message,
            "listing_id": id,
            "page_type": page_type
          };

	  
	  /*Send To Friend Form Validation*/ 
	  var form = $( "#send-to-friend-form" );   
	  form.validate();
	  if(!form.valid()){
	      return false;
	  }
      var is_captcha_valid = captcaValidation("#frnd-captcha_control");
        if( ! is_captcha_valid){
          return false;
      }

      url = unit_info.building.send_to_friend;
      button.attr("disabled", "disabled");

      $("#send-frnd-notification").text("Sending...");
      $.post(url, fdata, function(response, status, xhr){
          button.attr("disabled", false);
          $("#send-frnd-notification").text("Thank You, Your Message Has Been Sent.");
          setTimeout(function(){
            $("#send-frnd-notification").text("");
          }, 300);
          $("#send-to-friend-modal").modal("hide");
      });
      return false;
    });
    
  });



// <!-- mapbox on the unit details page -->
var map = L.mapbox.map('map', 'rentplatform.map-kwvio7ve')
    .setView([ unit_info.building.lat, unit_info.building.lng], 15);//[40, -74.50], 9);

var geoJson =[{
    // this feature is in the GeoJSON format: see geojson.org
    // for the full specification
    type: 'Feature',
    geometry: {
        type: 'Point',
        // coordinates here are in longitude, latitude order because
        // x, y is the standard for GeoJSON and many formats
        coordinates: [ unit_info.building.lng, unit_info.building.lat ]
    },
    properties: {
      "icon" : {
                "iconUrl": unit_info.building.marker_icon_url,
                "iconSize": [20, 31],
                "iconAnchor": [18, 47],
                "popupAnchor": [-95, -25]
        
              },
      "image": unit_info.building.marker_icon_url,
//        title: ('{{ unit_info.building }}').toUpperCase(),
//        description: '{{ unit_info.building.address }}',
        // one can customize markers by adding simplestyle properties
        // http://mapbox.com/developers/simplestyle/
//        'marker-size': 'large',
        //'marker-color': '#f0a'
    }
}];

// Set a custom icon on each marker based on feature properties
map.markerLayer.on('layeradd', function(e) {
    var marker = e.layer,
        feature = marker.feature;

    marker.setIcon(L.icon(feature.properties.icon));
});
map.removeControl(map.infoControl);
// Add features to the map
map.markerLayer.setGeoJSON(geoJson);

