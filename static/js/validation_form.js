$(document).ready(function() {
	
$('[placeholder]').focus(function() {
  var input = $(this);
  if (input.val() == input.attr('placeholder')) {
    input.val('');
    input.removeClass('placeholder');
  }
}).blur(function() {
  var input = $(this);
  if (input.val() == '' || input.val() == input.attr('placeholder')) {
    input.addClass('placeholder');
    input.val(input.attr('placeholder'));	
  }
}).blur();

$("#contact_from_id").validate({
	
	
		rules: {
			email: {
				required: true,
				email: true
			},
		first_name:{ required: true},
		last_name:{ required: true},		
		phone:{ required: true}
		}
		});



$("#contact_from_b_id").validate({
	
	
		rules: {
			email: {
				required: true,
				email: true
			},
		first_name:{ required: true},
		last_name:{ required: true},		
		phone:{ required: true}
		}
		});

	$.validator.addMethod("placeholder", function(value, element) {
	  return value!=$(element).not('#adress').not('#city').not('#state').not('#zip').not('#adress').not('#textarea').attr("placeholder");
	}, $.validator.messages.required);

});