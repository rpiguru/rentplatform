// <!-- Contact agent and send to friend functions used on unit details page -->

 
function randomNumber(min, max){
    min = min || 1;
    max = max || 10;
  return (Math.floor(Math.random() * (max - min + 1)) + min);
  }

  function setCaptcha(selector){
    var captchaDiv = $(selector);
    captchaDiv.find("#cap_num_1").text(randomNumber());
    captchaDiv.find("#cap_num_2").text(randomNumber());
  }
    
  function captcaValidation(selector){
    var captchaDiv = $(selector);
    captchaDiv.find("#captcha_error").hide(300);
    captchaDiv.removeClass("error");
    var userInput = captchaDiv.find("input#captcha").val();
    var int_1 = parseInt(captchaDiv.find("#cap_num_1").text());
    var int_2 = parseInt(captchaDiv.find("#cap_num_2").text());
    var sum = int_1 + int_2;
    if(userInput == sum){
      captchaDiv.find("input#captcha").val("");
      return true;
    }else{
      captchaDiv.addClass("error");
      captchaDiv.find("#captcha_error").show(300);
      return false;
    }
  }

//block to handle lightbox for contact Agent feature


 $(function contactAgent(){
    //CONTACT AGENT
    $(".building-listing-contact").on('click', function(e){
      e.preventDefault();
       var apt = $(this).attr('data-apt');
       var building = $(this).attr('data-building');
       var id = $(this).attr('data-unit-id');
       var phone = $(this).attr('data-phone');
       $(".building-listing-phone").text(phone);
       $("form[name=contact-agent-form]").find("input[name=listing_id]").val(id);
       $("form[name=contact-agent-form]").data("building", building);
       $("form[name=contact-agent-form]").data("apt", apt);
       $("#contact-agent-modal-message").val("I am interested in apt " + apt + " at " + building + ".");
       setCaptcha("#agent-captcha_control");
      $("#contact-agent-modal").modal();
    });

	
    //ACTION TO SEND A MAIL WHEN SEND BUTTON IS CLICKED
    $("#contact-agent-action").on('click', function(e){
      var button = $(this);
      var data = {};
      
      data["firstname"] = $("form[name=contact-agent-form]").find("input[name=firstname]").val();
      data["lastname"] = $("form[name=contact-agent-form]").find("input[name=lastname]").val();
      data["email"] = $("form[name=contact-agent-form]").find("input[name=email]").val();
      data["telephone"] = $("form[name=contact-agent-form]").find("input[name=telephone]").val();
      data["message"] = $("form[name=contact-agent-form]").find("textarea[name=message]").val();
      data["listing_id"] = $("form[name=contact-agent-form]").find("input[name=listing_id]").val();
      data["page_type"] = $("form[name=contact-agent-form]").find("input[name=page_type]").val();
      building = $("form[name=contact-agent-form]").data("building");
      apt = $("form[name=contact-agent-form]").data("apt");
      
      /*
      if(! data["firstname"] || ! data["lastname"] || ! data["email"] || ! data["telephone"] || ! data["message"]){
        alert("Please fill the complete form.");
        return false;
      }*/
      /*Contact Agent Form Validation*/ 
      var form = $( "#contact-agent-form" );   
      form.validate();
      if(!form.valid()){
        return false;
      }
     
      var is_captcha_valid = captcaValidation("#agent-captcha_control");
      if( ! is_captcha_valid){
        return false;
      }
      var url = building_data.building.email_agent_url;
      button.attr("disabled", "disabled");
      
     
      $("#contact-agent-notification").text("Sending...");
      $.post(url, data, function(response, status, xhr){
        if(data["page_type"]=="building"){
          var eventLabel = building;
          var eventCategory = "Contact Agent for Building";
        }else{
          var eventLabel = apt + " - " + building;
          var eventCategory = "Contact Agent for Listing ";
        }
        
   
      	//building_name = $("form#contact-agent-form").data("building");
      	//apt_num = $("form#contact-agent-form").data("apt");
        ga('send', { 	
            'hitType': 'event',
            'eventCategory':eventCategory,   // Required.
            'eventAction': 'click',      // Required.
            'eventLabel': eventLabel,
            'page': location.pathname
          }); 
          button.attr("disabled", false);
          
          $("#contact-agent-notification").text("Thank You, Your Message Has Been Sent.");
          setTimeout(function(){
            $("#contact-agent-notification").text("");
          }, 300);

         $("#contact-agent-modal").modal('hide');
      });
    });
    
    
    //CLOSE CONTACT AGENT
    $(".close-contact-agent").click(function(e){
      e.preventDefault();
      $("#contact-agent-modal").modal('hide');
    });

  });



// to open the gallry image
$(function(){
        var buildingData = building_data.building.photos;
        $(".box").on("click", function(event){
          var currentImage = $(event.target).data("num");
          $.fancybox.open(buildingData, {
            prevEffect  : 'none',
            nextEffect  : 'none',
            index:currentImage
          });
        });
        
});


// to load the map for building detail view
function building_map(){
var map = L.mapbox.map('map', 'rentplatform.map-kwvio7ve')
    .setView([building_data.building.lat, building_data.building.lng], 15);
    
var features = [];

features.push({
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: [building_data.building.lng, building_data.building.lat]
    },
    properties: {
      "icon" : {
        "iconUrl": building_data.building.marker_url,
        "iconSize": [20, 31],
        "iconAnchor": [18, 47],
        "popupAnchor": [-95, -25]
      },
      "image": building_data.building.marker_url,
      "id":building_data.building.id,
      "name":building_data.building.name,
    }
});
map.markerLayer.on('layeradd', function(e) {
    var marker = e.layer,
    feature = marker.feature;
    marker.setIcon(L.icon(feature.properties.icon));
});

var geoJson = {
  type: 'FeatureCollection',
  "features": features
};
map.markerLayer.setGeoJSON(geoJson);
}


// function to send message on contact page

  $("#contact_from_id").submit(function(e){
    e.preventDefault();
    var form = $( "#contact_from_id" );
    form.validate();
    if(!form.valid()){
        return false;
    }
    $("#con_form_submit_id").attr("disabled", "disabled");
    
    $(".form_box_3").append('<p class="sending">SENDING...</p>');
    var data = {};
    $("input[type='text'],input[type='email'],textarea").each(function(){
        data[$(this).attr('name')] = $(this).val();
    });
    data['contact'] = $("select option:selected").val();
    $.post('/contact/submit/',data,function(data){
      $("#con_form_submit_id").attr("disabled", null);
        window.location.href = '/contact/thank_you/';
        $("input[type='text'],input[type='email'],textarea").each(function(){
            $(this).val('');
            $(".sending").remove();
    });
    });
    return false;
  });